<?php include 'include/header.php' ?>
<!-- template_signup_wrapper::start  -->
<div class="template_signup_wrapper template_order_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo text-center">
                    <a href="#">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-12 mb_15">
                <div class="banner template_bg_3">
                    <div class="banner_text">
                        <h3 class="dark_text" >Your order has <br>
                            been delivered!</h3>
                        <a href="#" class="theme_btn">Order Status</a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="order_placed_info white_bg mb_15">
                    <h4>Hi Robert Downey,</h4>
                    <p class="mb_25" >We are pleased to inform that your order <span class="f_w_600 theme_text3">#611316362436256</span> has been delivered.</p>
                    <p  class="mb-0">We hope you are enjoying your recent purchase! Once you have a chance, we would love 
                    to hear your shopping experience to keep us constantly improving.</p>
                
                </div>
            </div>
            <div class="col-12">
                <div class="delivery_detais white_bg mb_15">
                    <h4 class="font_20 f_w_700 mb_10" >Delivery Details</h4>
                    <ul class="delivery_list">
                        <li><span>Name:</span> <p>Robert Downey JR.</p></li>
                        <li><span>Address:</span> <p>34 New Clity 5655, Excel Tower, OPG Rpad, 4538FH</p></li>
                        <li><span>Phone:</span> <p>+880 015 2147 5317</p></li>
                        <li><span>Email:</span> <p>spn12@spondonit.com</p></li>
                    </ul>
                </div>
            </div>
            <div class="col-12">
                <div class="order_details white_bg mb_15 padding_35">
                    <h4 class="font_20 f_w_700" >Order Details</h4>
                    <p class="font_14 text-uppercase f_w_600" >PACKAGE 1</p>
                    <p>Sold by Khadiza Electronics <br>
                    Estimated delivery between 19 Nov - 25 Nov, 2020</p>
                    <div class="order_products">
                        <div class="thumb">
                            <img src="img/template/product.jpg" alt="">
                        </div>
                        <div class="order_content w-100">
                            <p>XUNDD Protective tablet Case for new 
                            iPad Pro 12.9 inch 2020.</p>
                            <span class="prise_text">Price: $1200</span>
                            <span class="quentity_count">Quantity: 1</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="product_prise_list white_bg mb_15">
                    <div class="product_prise_single">
                        <span class="prist_list_text">Price</span>
                        <span class="prist_list_prise">$168.00</span>
                    </div>
                    <div class="product_prise_single">
                        <span class="prist_list_text">Discount</span>
                        <span class="prist_list_prise">$00.00</span>
                    </div>
                    <div class="product_prise_single">
                        <span class="prist_list_text">Delivery fee	</span>
                        <span class="prist_list_prise">Free</span>
                    </div>
                    <div class="product_prise_single">
                        <span class="prist_list_text">Total</span>
                        <span class="prist_list_prise">$1200</span>
                    </div>
                    <!-- total  -->
                    <div class="border_1px"></div>
                    <div class="product_prise_single">
                        <span class="prist_list_text">Shipping Option	</span>
                        <span class="prist_list_prise">STANDARD</span>
                    </div>
                    <div class="product_prise_single mb-0">
                        <span class="prist_list_text">Paid via	</span>
                        <span class="prist_list_prise">Cash On Delivery</span>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="order_help white_bg mb_15">
                    <h4>NEED HELP?</h4>
                    <div class="order_help_single">
                        <h5>Will there be delivery delay due to COVID,19? </h5>
                        <p>Due to the government’s recent COVID-19 restrictions, your order can be delayed. 
                        You will be notified through sms or email in case of delay.</p>
                    </div>
                    <div class="order_help_single">
                        <h5>Can I change my shipping address after I have placed the order?</h5>
                        <p>Unfortunately, you can’t change the address, but you may cancel and 
                        re-order with right address.</p>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="themplate_download_app white_bg text-center mt-0">
                    <h4 class="font_24 f_w_700 mb-0">Save More on App!</h4>
                    <p class="f_w_500">Get exclusive discounts, voucher & daily <br>
                        flash sales on the app.</p>
                    <div class="download_Links">
                        <a href="#">
                            <img src="img/template/google.svg" alt="">
                        </a>
                        <a href="#">
                            <img src="img/template/apple.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="template_footer">
                    <div class="template_footer_text">
                        <p>You have received this email because you or someone else has confirmed the email address. This would like 
                        to receive email communication from InfixVuci. We will never share your personal information 
                        (such as your email address with any other 3rd party without your consent).</p>
                        <p>This email was sent by: Tajwar Centre House No: 40 Baria Sreet 133/2 NY City, United States.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- template_signup_wrapper::end  -->


<?php include 'include/footer.php' ?>