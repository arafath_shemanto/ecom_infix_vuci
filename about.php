<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Our Company</h3>
                        <p><a href="index.php">Home </a>/ <a href="product.php">New Arrivals</a> /  Product details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- about_area::start  -->
<div class="about_area">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-6">
                <div class="about_imges">
                    <div class="img_1">
                        <img src="img/about/1.jpg" alt="">
                    </div>
                    <div class="img_2">
                        <img src="img/about/2.jpg" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="about_content">
                    <span class="font_14 text-uppercase theme_text d-block f_w_600" >About our company</span>
                    <h3>Crafting beautiful stuff with 
                    our own hands and the help 
                    from useful tool.</h3>
                    <p class="mb_25">Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Sed ut perspiciatis.</p>
                    <p class="mb-0">Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about_area::end  -->

<!-- welcome__service_wrapper::start  -->
<div class="welcome__service_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title3 text-center mb_60">
                    <span>Welcome to InfixVuci</span>
                    <h3>Life Full of Creativity</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="single_services mb_30">
                    <div class="thumb">
                        <img src="img/about/1.png" alt="">
                    </div>
                    <div class="service_content text-center">
                        <h3>Who we are?</h3>
                        <p>Cur tantas regiones barbarorum obiit maria
                        transmi Uterque enim summo bono fruitur idest 
                        voluptate barbarorum pedibu</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single_services mb_30">
                    <div class="thumb">
                        <img src="img/about/2.png" alt="">
                    </div>
                    <div class="service_content text-center">
                        <h3>How we work?</h3>
                        <p>Cur tantas regiones barbarorum obiit maria
                        transmi Uterque enim summo bono fruitur idest 
                        voluptate barbarorum pedibu</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="single_services mb_30">
                    <div class="thumb">
                        <img src="img/about/3.png" alt="">
                    </div>
                    <div class="service_content text-center">
                        <h3>Product Quality</h3>
                        <p>Cur tantas regiones barbarorum obiit maria
                        transmi Uterque enim summo bono fruitur idest 
                        voluptate barbarorum pedibu</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- welcome__service_wrapper::end  -->

<!-- about_video_wrapper::start  -->
<div class="about_video_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="about_video_inner">
                    <a href="#" class="play_button">
                        <i class="ti-control-play"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about_video_wrapper::end  -->

<!-- quality__info_area::start  -->
<div class="quality__info_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="quality__title mb_40">
                    <h3>This is the difference <br>
                    when you shop on <br>
                    InfixVuci.</h3>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="delivery_info">
                    <div class="single_del_info">
                        <div class="icon">
                            <img src="img/about/car.png" alt="">
                        </div>
                        <div class="del_content">
                            <h4>Free Delivery</h4>
                            <p>Cur tantas regiones barbarorum obiit 
                            Maria transmi Uterque enim summos 
                            bono fruitur idest.</p>
                        </div>
                    </div>
                    <div class="single_del_info">
                        <div class="icon">
                            <img src="img/about/clock.png" alt="">
                        </div>
                        <div class="del_content">
                            <h4>90 DAYS RETURN</h4>
                            <p>Cur tantas regiones barbarorum obiit 
                            Maria transmi Uterque enim summos 
                            bono fruitur idest.</p>
                        </div>
                    </div>
                    <div class="single_del_info">
                        <div class="icon">
                            <img src="img/about/payment.png" alt="">
                        </div>
                        <div class="del_content">
                            <h4>SECURE PAYMENT</h4>
                            <p>Cur tantas regiones barbarorum obiit 
                            Maria transmi Uterque enim summos 
                            bono fruitur idest.</p>
                        </div>
                    </div>
                    <div class="single_del_info">
                        <div class="icon">
                            <img src="img/about/help.png" alt="">
                        </div>
                        <div class="del_content">
                            <h4>24/7 Help Center</h4>
                            <p>Cur tantas regiones barbarorum obiit 
                            Maria transmi Uterque enim summos 
                            bono fruitur idest.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- quality__info_area::end  -->

<!-- team_member_area::start  -->
<div class="team_member_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title3 text-center mb_60">
                    <span>Team Member</span>
                    <h3>People Behind Our Products</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single_member mb_40">
                    <div class="thumb">
                        <img src="img/team/1.jpg" alt="">
                    </div>
                    <div class="member_info">
                        <h4>Robert Pattinson</h4>
                        <p>Founder & CEO</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single_member mb_40">
                    <div class="thumb">
                        <img src="img/team/2.jpg" alt="">
                    </div>
                    <div class="member_info">
                        <h4>Johnny Depp</h4>
                        <p>Founder & CEO</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single_member mb_40">
                    <div class="thumb">
                        <img src="img/team/3.jpg" alt="">
                    </div>
                    <div class="member_info">
                        <h4>Jason Statham</h4>
                        <p>Founder & CEO</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single_member mb_40">
                    <div class="thumb">
                        <img src="img/team/4.jpg" alt="">
                    </div>
                    <div class="member_info">
                        <h4>Bradley Cooper</h4>
                        <p>Founder & CEO</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- team_member_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>