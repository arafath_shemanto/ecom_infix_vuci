<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- faq_banner::start  -->
<div class="faq_banner_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="faq_banner_inner">
                    <div class="faq_banner_text">
                        <h3>How we can help you?</h3>
                        <div class="input-group theme_search_field ">
                            <div class="input-group-prepend">
                                <button class="btn" type="button" id="button-addon2"> <i class="ti-search"></i> </button>
                            </div>
                            <input type="text" class="form-control" placeholder="Search for course, skills and Videos">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- faq_banner::end  -->

<!-- faq_area::start  -->
<div class="faq_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="nav faq_tab" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">General Questions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Delivery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Order</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="Payment-tab" data-toggle="tab" href="#Payment" role="tab" aria-controls="Payment" aria-selected="false">Payment</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                <!-- content  -->
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <h3 class="font_30 f_w_700 mb_25">General Questions</h3>
                        <div class="theme_according mb_30" id="accordion1">
                            <div class="card">
                                <div class="card-header pink_bg" id="headingFour">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link text_white collapsed" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">Are your product for commercial use?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseFour" aria-labelledby="headingFour" data-parent="#accordion1">
                                    <div class="card-body">
                                        <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingFive">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">Placing your order to cancel or change your mind completely free or charge?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseFive" aria-labelledby="headingFive" data-parent="#accordion1">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix" aria-expanded="false" aria-controls="collapseSix">Which show our newest and best-selling pieces in real life?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix" aria-labelledby="headingSix" data-parent="#accordion1">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix4">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix4" aria-expanded="false" aria-controls="collapseSix">Can you provide a fabric sample?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix4" aria-labelledby="headingSix4" data-parent="#accordion1">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix5">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix5" aria-expanded="false" aria-controls="headingSix5">Do you have a furniture collection service?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix5" aria-labelledby="headingSix5" data-parent="#accordion1">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix6">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix6" aria-expanded="false" aria-controls="headingSix6">Do you have all your items on display in your showroom?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix6" aria-labelledby="headingSix6" data-parent="#accordion1">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ content  -->
            </div>
            <div class="tab-pane fade " id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <!-- content  -->
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <h3 class="font_30 f_w_700 mb_25">General Questions</h3>
                        <div class="theme_according mb_30" id="accordion2">
                            <div class="card">
                                <div class="card-header pink_bg" id="headingFour1">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link text_white collapsed" data-toggle="collapse" data-target="#collapseFour1" aria-expanded="false" aria-controls="collapseFour1">Are your product for commercial use?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseFour1" aria-labelledby="headingFour1" data-parent="#accordion2">
                                    <div class="card-body">
                                        <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingFive1">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseFive1" aria-expanded="false" aria-controls="collapseFive1">Placing your order to cancel or change your mind completely free or charge?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseFive1" aria-labelledby="headingFive1" data-parent="#accordion2">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix1">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix1" aria-expanded="false" aria-controls="collapseSix">Which show our newest and best-selling pieces in real life?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix1" aria-labelledby="headingSix1" data-parent="#accordion2">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix41">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix41" aria-expanded="false" aria-controls="collapseSix41">Can you provide a fabric sample?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix41" aria-labelledby="headingSix41" data-parent="#accordion2">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix51">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix51" aria-expanded="false" aria-controls="headingSix51">Do you have a furniture collection service?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix51" aria-labelledby="headingSix51" data-parent="#accordion2">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix61">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix61" aria-expanded="false" aria-controls="headingSix61">Do you have all your items on display in your showroom?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix61" aria-labelledby="headingSix61" data-parent="#accordion2">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ content  -->
            </div>
            <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                <!-- content  -->
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <h3 class="font_30 f_w_700 mb_25">General Questions</h3>
                        <div class="theme_according mb_30" id="accordion3">
                            <div class="card">
                                <div class="card-header pink_bg" id="headingFourOrder">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link text_white collapsed" data-toggle="collapse" data-target="#collapseFourOrder" aria-expanded="false" aria-controls="collapseFourOrder">Are your product for commercial use?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseFourOrder" aria-labelledby="headingFourOrder" data-parent="#accordion3">
                                    <div class="card-body">
                                        <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingFiveOrder">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseFiveOrder" aria-expanded="false" aria-controls="collapseFiveOrder">Placing your order to cancel or change your mind completely free or charge?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseFiveOrder" aria-labelledby="headingFiveOrder" data-parent="#accordion3">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSixOrder">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSixOrder" aria-expanded="false" aria-controls="collapseSixOrder">Which show our newest and best-selling pieces in real life?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSixOrder" aria-labelledby="headingSixOrder" data-parent="#accordion3">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix4Order">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix4Order" aria-expanded="false" aria-controls="collapseSixOrder">Can you provide a fabric sample?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix4Order" aria-labelledby="headingSix4Order" data-parent="#accordion3">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix5Order">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix5Order" aria-expanded="false" aria-controls="headingSix5Order">Do you have a furniture collection service?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix5Order" aria-labelledby="headingSix5Order" data-parent="#accordion3">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix6Order">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix6Order" aria-expanded="false" aria-controls="headingSix6Order">Do you have all your items on display in your showroom?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix6Order" aria-labelledby="headingSix6Order" data-parent="#accordion3">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ content  -->
            </div>
            <div class="tab-pane fade  show active" id="Payment" role="tabpanel" aria-labelledby="Payment-tab">
                <!-- content  -->
                <div class="row justify-content-center">
                    <div class="col-xl-6">
                        <h3 class="font_30 f_w_700 mb_25">General Questions</h3>
                        <div class="theme_according mb_30" id="accordion4">
                            <div class="card">
                                <div class="card-header pink_bg" id="headingFourPayment">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link text_white collapsed" data-toggle="collapse" data-target="#collapseFourPayment" aria-expanded="false" aria-controls="collapseFourPayment">Are your product for commercial use?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseFourPayment" aria-labelledby="headingFourPayment" data-parent="#accordion4">
                                    <div class="card-body">
                                        <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingFivePayment">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseFivePayment" aria-expanded="false" aria-controls="collapseFivePayment">Placing your order to cancel or change your mind completely free or charge?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseFivePayment" aria-labelledby="headingFivePayment" data-parent="#accordion4">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSixPayment">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSixPayment" aria-expanded="false" aria-controls="collapseSixPayment">Which show our newest and best-selling pieces in real life?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSixPayment" aria-labelledby="headingSixPayment" data-parent="#accordion4">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix4Payment">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix4Payment" aria-expanded="false" aria-controls="collapseSixPayment">Can you provide a fabric sample?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix4Payment" aria-labelledby="headingSix4Payment" data-parent="#accordion4">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix5Payment">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix5Payment" aria-expanded="false" aria-controls="headingSix5Payment">Do you have a furniture collection service?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix5Payment" aria-labelledby="headingSix5Payment" data-parent="#accordion4">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header pink_bg" id="headingSix6Payment">
                                    <h5 class="mb-0">
                                    <button class="btn btn-link collapsed text_white" data-toggle="collapse" data-target="#collapseSix6Payment" aria-expanded="false" aria-controls="headingSix6Payment">Do you have all your items on display in your showroom?</button>
                                    </h5>
                                </div>
                                <div class="collapse" id="collapseSix6Payment" aria-labelledby="headingSix6Payment" data-parent="#accordion4">
                                    <div class="card-body">
                                    <h5> <i class="ti-minus"></i> Placing your order to cancel or change your mind completely free or charge?</h5>
                                        <p>Konseptbizden believes everyone should live in a home they love. Through hnology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings décor home improvement.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ content  -->
            </div>
        </div>
    </div>
</div>
<!-- faq_area::end  -->

<!-- cta_area::start -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>