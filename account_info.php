<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Update Account Info</h3>
                        <p><a href="index.php">Home </a>/ <a href="#">My Account</a> / Update account info</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- my_address_area::start  -->
<div class="my_address_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="my_address_inner ">
                    <div class="notify_text mt-2">
                        <i class="ti-info-alt"></i>
                        <p>The following addresses will be used on the checkout by default.</p>
                    </div>
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="single_addressLine mb_20">
                                <h3 class="mb_15 d-flex align-items-center">Address Line <a class="font_14 f_w_500 theme_text ml_40 edit_text" href="account_info_edit.php">EDIT</a></h3>
                                <span class="text-uppercase font_14 f_w_500 d-block mb_20 mt-0 mute_text">Default shipping address</span>
                                <h4 class="font_18 f_w_600">Robert Downey JR.</h4>
                                <p class="mb-2">2593 Timbercrest Road, Chisana, Alaska 
                                    Badalas United State.</p>
                                <p class="mb-0">info@spondonit.com <br>
                                +880 1737 499 497</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- my_address_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>