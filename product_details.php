<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Wichita Hydraulic Table</h3>
                        <p><a href="index.php">Home </a>/ <a href="product.php">New Arrivals</a> /  Product details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- product_details_wrapper::start  -->
<div class="product_details_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="products_imgs mb_30">
                    <div class="single_product_image">
                        <a class="popup-image" href="img/product_details/1.jpg">
                            <img src="img/product_details/1.jpg" alt="">
                        </a>
                    </div>
                    <div class="single_product_image">
                    <a class="popup-image" href="img/product_details/2.jpg">
                            <img src="img/product_details/2.jpg" alt="">
                        </a>
                    </div>
                    <div class="single_product_image">
                    <a class="popup-image" href="img/product_details/3.jpg">
                            <img src="img/product_details/3.jpg" alt="">
                        </a>
                    </div>
                    <div class="single_product_image">
                    <a class="popup-image" href="img/product_details/4.jpg">
                            <img src="img/product_details/4.jpg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="product_content_details mb_30">
                    <span class="theme_btn green_btn cursor_default ">In Stock</span>
                    <h3>Wichita Hydraulic Table</h3>
                    <div class="product_ratings">
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <span> (6 customer reviews)</span>
                    </div>
                    <p class="details_text" >Ut ratione qui sunt officiis quo cum ut. Quas aliquam cum ipsam facerequaerat Et et aut quibusdam deleniti nisi sunt rerum. Consequatur molestiae address in molestias eius unde facere est architecto.</p>
                    <div class="product_info">
                        <div class="single_pro_varient">
                            <span class="font_14 f_w_600 theme_text3  text-uppercase" >Color:</span>
                            <div class="color_List d-flex">
                                <label class="round_checkbox blue_check  d-flex">
                                    <input name="color_filt" type="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="round_checkbox paste_check  d-flex">
                                    <input name="color_filt" type="radio">
                                    <span class="checkmark"></span>
                                </label>
                                <label class="round_checkbox violate_check  d-flex">
                                    <input name="color_filt" type="radio">
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="single_pro_varient">
                            <span class="font_14 f_w_600 theme_text3  text-uppercase" >Size    :</span>
                            <ul class="product_size">
                                <li>M</li>
                                <li>L</li>
                                <li>Xl</li>
                            </ul>
                        </div>
                        <div class="single_pro_varient">
                            <span class="font_14 f_w_600 theme_text3  text-uppercase" >QTY:</span>
                            <div class="product_number_count" data-target="amount-1">
                                <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                <input id="amount-1" class="count_single_item input-number" type="text" value="1">
                                <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                            </div>
                        </div>
                        <div class="add_wishCard">
                            <a href="#" class="theme_btn mr_15 add_to_cart">Add to Cart</a>
                            <a class="add_wish_list" href="wishlist.php"> <i class="far fa-heart"></i> Add to Wishlist</a>
                        </div>
                    </div>
                    <div class="pro_branding_info">
                        <div class="single_branding">
                            <span>Category<span>:</span></span>
                            <p>Beds, Decoration</p>
                        </div>
                        <div class="single_branding">
                            <span>SKU<span>:</span></span>
                            <p>60983</p>
                        </div>
                        <div class="single_branding">
                            <span>Brand <span>:</span></span>
                            <p>Salink</p>
                        </div>
                        <div class="single_branding">
                            <span>Share<span>:</span></span>
                            <ul class="social_links">
                                <li><a class="fab fa-facebook-f facebook_bg" href="#"></a></li>
                                <li><a class="fab fa-twitter twitter_bg" href="#"></a></li>
                                <li><a class="fab fa-linkedin-in linkedin_bg" href="#"></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- product_details_wrapper::end  -->

<!-- product_des_review_wrapper::start  -->
<div class="product_des_review_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-11">
                <ul class="nav shop_tab" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Description</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Reviews (90)</a>
                    </li>
                </ul>
                <div class="tab-content shop_tab_content" id="myTabContent">
                    <div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
                        <!-- content  -->
                        <div class="description_content">
                            <p class="mb_25">Ex eius quo recusandae quia voluptatem eius tempore. Provident dolores ut enim explicabo magnam magnam dolor. Sit delectus qui sed et dolorem ex dolorem.Qui corrupti et nam quaerat doloremque et. Repellendus culpa minus debitis rerum. Velit ut odit sint sequi dolorum et doloribus. Ut ad ea harum quas adipisci.Nihil id assumenda aut harum quasi id. Iusto animi aspernatur voluptas exercitationem et consequatur ipsum. Et enim dolorum corrupti in est.  </p>
                            <p>Reiciendis ullam corporis perspiciatis libero fuga. Repudiandae totam vel ut quia porro cumque.Aut similique non qui est. Et fuga doloremque harum consequatur illo facere. Molestiae voluptatem repudiandae iure inventore quidem nesciunt sint. Labore magnam magnam omnis.Quo corporis voluptatem et magnam. Quia ex laboriosam qui quasi praesentium libero est. Excepturi odit occaecati temporibus laborum rerum illum. Vel et mollitia dolor aliquam dolorem eaque</p>
                            <div class="Specifications">
                                <h4>Specifications</h4>
                                <p class="mb_25" >Ex eius quo recusandae quia voluptatem eius tempore. Provident dolores ut enim explicabo magnam magnam dolor. Sit delectus qui sed et dolorem ex dolorem.Qui corrupti et nam quaerat doloremque et. Repellendus culpa minus debitis rerum. Velit ut odit sint sequi dolorum et doloribus. Ut ad ea harum quas adipisci.Nihil id assumenda aut harum quasi id. Iusto animi aspernatur voluptas exercitationem et consequatur ipsum. Et enim dolorum corrupti in est.</p>
                                <div class="Specifications_list">
                                    <div class="single_specif">
                                        <span>Dimension (L x W x H) <span>:</span></span>
                                        <p>10 x 20 x 30 Inch</p>
                                    </div>
                                    <div class="single_specif">
                                        <span>Weight <span>:</span></span>
                                        <p>340 Gram</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content  -->
                    </div>
                    <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <!-- content  -->
                        <div class="product_reviews_wrapper">
                            <div class="details_title">
                                <h4 class="font_22 f_w_700">Student Feedback</h4>
                                <p class="font_16 f_w_400">Building your Cloud and Data Centre career</p>
                            </div>
                            <div class="course_feedback">
                                <div class="course_feedback_left">
                                    <h2>4.7</h2>
                                    <div class="feedmak_stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span>Course Rating</span>
                                </div>
                                <div class="feedbark_progressbar">
                                    <div class="single_progrssbar">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 70%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                        <div class="rating_percent d-flex align-items-center">
                                            <div class="feedmak_stars d-flex align-items-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <span>70%</span>
                                        </div>
                                    </div>
                                    <div class="single_progrssbar">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 25%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                        <div class="rating_percent d-flex align-items-center">
                                            <div class="feedmak_stars d-flex align-items-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <span>20%</span>
                                        </div>
                                    </div>
                                    <div class="single_progrssbar">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 20%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                        <div class="rating_percent d-flex align-items-center">
                                            <div class="feedmak_stars d-flex align-items-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                
                                            </div>
                                            <span>10%</span>
                                        </div>
                                    </div>
                                    <div class="single_progrssbar">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                        <div class="rating_percent d-flex align-items-center">
                                            <div class="feedmak_stars d-flex align-items-center">
                                                <i class="fas fa-star"></i>
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <span>01%</span>
                                        </div>
                                    </div>
                                    <div class="single_progrssbar">
                                        <div class="progress">
                                            <div class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                        <div class="rating_percent d-flex align-items-center">
                                            <div class="feedmak_stars d-flex align-items-center">
                                                <i class="fas fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                                <i class="far fa-star"></i>
                                            </div>
                                            <span>01%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="course_cutomer_reviews">
                                <div class="row">
                                    <div class="col-xl-8">
                                        <div class="details_title">
                                            <h4 class="font_22 f_w_700">Reviews</h4>
                                            <p class="font_16 f_w_400">Building your Cloud and Data Centre career</p>
                                        </div>
                                        <div class="customers_reviews">
                                            <div class="single_reviews">
                                                <div class="thumb">
                                                    ks
                                                </div>
                                                <div class="review_content">
                                                    <h4 class="f_w_700" >Kristen Stewart</h4>
                                                    <div class="rated_customer d-flex align-items-center">
                                                        <div class="feedmak_stars">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <span>2 weeks ago</span>
                                                    </div>
                                                    <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing. Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar flap chest pockets topline stitching cropped.</p>
                                                </div>
                                            </div>
                                            <div class="single_reviews">
                                                <div class="thumb">
                                                    EW
                                                </div>
                                                <div class="review_content">
                                                    <h4 class="f_w_700" >Emma Watson</h4>
                                                    <div class="rated_customer d-flex align-items-center">
                                                        <div class="feedmak_stars">
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                            <i class="fas fa-star"></i>
                                                        </div>
                                                        <span>2 weeks ago</span>
                                                    </div>
                                                    <p>See-through delicate embroidered organza blue lining luxury acetate-mix stretch pleat detailing. Leather detail shoulder contrastic colour contour stunni silhouette working peplum. Statement buttons cover-up tweaks patch pockets perennia lapel collar flap chest pockets topline stitching cropped.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- content  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- product_des_review_wrapper::end  -->

<!-- sujjested_prosuct_area::start  -->
<div class="sujjested_prosuct_area">
    <div class="container">
        <div class="seperator_line"></div>
        <div class="row">
            <div class="col-12">
                <div class="main_title mb_30">
                    <h3 class="mb-0">You May Also Like</h3>
                </div>
            </div>
        </div>
        <div class="row no-gutters sujjested_prosuct_wrap">
            <div class="col-xl-3 col-md-6">
                <div class="product_wized mb_30">
                    <div class="thumb">
                        <img src="img/product/5.jpg" alt="">
                        <div class="product_action">
                            <div class="product_action_inner">
                                <a href="#">
                                    <i class="ti-heart"></i>
                                </a>
                                <a href="#">
                                    <i class="ti-control-shuffle"></i>
                                </a>
                                <a href="#">
                                    <i class="ti-eye"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="product_wized_body">
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                        <a href="#">
                            <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                        </a>
                        <p class="font_16  theme_text f_w_500">$180.00</p>
                    </div>
                    <a href="#" class="add_to_cart">Add to Cart</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="product_wized mb_30">
                    <div class="thumb">
                        <img src="img/product/6.jpg" alt="">
                        <div class="product_action">
                            <div class="product_action_inner">
                                <a href="#">
                                    <i class="ti-heart"></i>
                                </a>
                                <a href="#">
                                    <i class="ti-control-shuffle"></i>
                                </a>
                                <a href="#">
                                    <i class="ti-eye"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="product_wized_body">
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                        <a href="#">
                            <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                        </a>
                        <p class="font_16  theme_text f_w_500">$180.00</p>
                    </div>
                    <a href="#" class="add_to_cart">Add to Cart</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="product_wized mb_30">
                    <div class="thumb">
                        <img src="img/product/7.jpg" alt="">
                        <div class="product_action">
                            <div class="product_action_inner">
                                <a href="#">
                                    <i class="ti-heart"></i>
                                </a>
                                <a href="#">
                                    <i class="ti-control-shuffle"></i>
                                </a>
                                <a href="#">
                                    <i class="ti-eye"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="product_wized_body">
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                        <a href="#">
                            <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                        </a>
                        <p class="font_16  theme_text f_w_500">$180.00</p>
                    </div>
                    <a href="#" class="add_to_cart">Add to Cart</a>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="product_wized mb_30">
                    <div class="thumb">
                        <img src="img/product/8.jpg" alt="">
                        <div class="product_action">
                            <div class="product_action_inner">
                                <a href="#">
                                    <i class="ti-heart"></i>
                                </a>
                                <a href="#">
                                    <i class="ti-control-shuffle"></i>
                                </a>
                                <a href="#">
                                    <i class="ti-eye"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="product_wized_body">
                        <div class="stars">
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                            <i class="fas fa-star"></i>
                        </div>
                        <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                        <a href="#">
                            <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                        </a>
                        <p class="font_16  theme_text f_w_500">$180.00</p>
                    </div>
                    <a href="#" class="add_to_cart">Add to Cart</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- sujjested_prosuct_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>