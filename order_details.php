<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Order Details</h3>
                        <p><a href="index.php">Home </a>/ My Account</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- my_order_details_area::start  -->
<div class="my_order_details_area">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-xl-4 col-lg-4">
                <div class="order_details_sidebar mb_30">
                    <div class="details_sidebar_widget">
                        <h3 class=" f_w_700 mb_20">My Account</h3>
                        <ul>
                            <li><a href="my_address.php">my address</a></li>
                            <li><a href="account_info.php">account details</a></li>
                            <li><a target="_blank" href="contact.php">support center</a></li>
                        </ul>
                    </div>
                    <div class="details_sidebar_widget">
                        <h3 class=" f_w_700 mb_20">My Order</h3>
                        <ul class="nav d-block" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Order Details</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="My_Order-tab" data-toggle="tab" href="#My_Order" role="tab" aria-controls="My_Order" aria-selected="false">My Orders</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">received list</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">pending list</a>
                            </li>
                        </ul>
                        <!-- <ul>
                            <li><a href="my_order_receive.php">received list</a></li>
                            <li><a href="my_order_pending.php">pending list</a></li>
                        </ul> -->
                    </div>
                </div>
            </div>
            <div class="col-xl-8 col-lg-8">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <!-- content  -->
                        <h3 class="font_30 f_w_700 mb_40">Order Details</h3>
                        <div class="row">
                            <div class="col-xl-4 col-md-6 col-lg-4 mb_30">
                                <div class="single_order">
                                    <div class="thumb">
                                        <img src="img/my_account/1.png" alt="">
                                    </div>
                                    <div class="order_meta text-center">
                                        <h5 class="font_18 f_w_700">Living Room Re-imagined</h5>
                                        <p class="font_16 f_w_500 theme_text mb-0">$180.00</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-lg-4 mb_30">
                                <div class="single_order">
                                    <div class="thumb">
                                        <img src="img/my_account/2.png" alt="">
                                    </div>
                                    <div class="order_meta text-center">
                                        <h5 class="font_18 f_w_700">Hair Growth and Volume</h5>
                                        <p class="font_16 f_w_500 theme_text mb-0">$180.00</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 col-lg-4 mb_30">
                                <div class="single_order">
                                    <div class="thumb">
                                        <img src="img/my_account/3.png" alt="">
                                    </div>
                                    <div class="order_meta text-center">
                                        <h5 class="font_18 f_w_700">Home Design Inspiration</h5>
                                        <p class="font_16 f_w_500 theme_text mb-0">$180.00</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="my_order_details_info">
                                    <div class="my_order_details_info_inner">
                                        <div class="single_order_details">
                                            <span>Subtotal: (3 Items)</span>
                                            <p>$168.00</p>
                                        </div>
                                        <div class="single_order_details">
                                            <span>Delivery Charge:</span>
                                            <p>$00.00</p>
                                        </div>
                                        <div class="single_order_details">
                                            <span>Discount (15%)</span>
                                            <p>- $69</p>
                                        </div>
                                    </div>
                                    <!-- Total  -->
                                    <div class="my_order_total">
                                        <span>Total</span>
                                        <p>$1689</p>
                                    </div>
                                </div>
                            </div>
                            <!-- address line  -->
                            <div class="col-12">
                                <div class="single_addressLine">
                                    <h3>Billing Address</h3>
                                    <p>Name: Robert Downey JR. <br>
                                        Email: robert@codethemes.com <br>
                                        Phone: +880 152 1475 317</p>
                                    <p>2593 Timbercrest Road, Chisana, Alaska <br> 
                                        Badalas United State.</p>
                                </div>
                            </div>
                        </div>
                        <!--/ content  -->
                    </div>
                    <div class="tab-pane fade" id="My_Order" role="tabpanel" aria-labelledby="My_Order-tab">
                        <!-- content  -->
                        <h3 class="font_30 f_w_700 mb_30">My Order</h3>
                        <div class="table-responsive">
                            <table class="table my_order_table mb-0">
                                <thead>
                                    <tr>
                                    <th scope="col">Order No.</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Items</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_1">Pending</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_2">Received</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_1">Pending</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_2">Received</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--/ content  -->
                    </div>
                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <!-- content  -->
                        <h3 class="font_30 f_w_700 mb_30">Received Order</h3>
                        <div class="table-responsive">
                            <table class="table my_order_table mb-0">
                                <thead>
                                    <tr>
                                    <th scope="col">Order No.</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Items</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_2">Received</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_2">Received</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_2">Received</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_2">Received</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_2">Received</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--/ content  -->
                    </div>
                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <!-- content  -->
                        <h3 class="font_30 f_w_700 mb_30">Pending Order</h3>
                        <div class="table-responsive">
                            <table class="table my_order_table mb-0">
                                <thead>
                                    <tr>
                                    <th scope="col">Order No.</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Items</th>
                                    <th scope="col">Total</th>
                                    <th scope="col">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_1">Pending</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_1">Pending</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_1">Pending</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_1">Pending</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_1">Pending</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                    <tr>
                                        <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                        <td>26 May 2020</td>
                                        <td> <span class="badge_1">Pending</span> </td>
                                        <td>02</td>
                                        <td>$360.00</td>
                                        <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--/ content  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- my_order_details_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>