<?php include 'include/header.php' ?>

<!-- template_signup_wrapper::start  -->
<div class="template_signup_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo text-center">
                    <a href="#">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-12">
                <div class="banner template_bg_1">
                    <div class="banner_text">
                        <h3>Your opinion does
                        Matter to us!</h3>
                        <a href="#" class="theme_btn">Shop Now</a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="template_main_content text-center">
                    <div class="product_review">
                        <h4>Hi Robert Downey,</h4>
                        <p>We need your help! <br> 
                            By sharing your experience about this purchase you can help other shoppers, 
                            like yourself, to pick the best products and sellers on InfixVuci.</p>
                        <div class="product_reiews">
                            <p>sports Overfly Bluetooth Headphones Meta…</p>
                            <div class="product_reiews_inner">
                                <div class="thumb">
                                    <img src="img/template/product.jpg" alt="">
                                </div>
                                <div class="product_reiews_content">
                                    <span>Start by Rating It :</span>
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <a href="#" class="theme_btn">Go to My Review</a>
                                </div>
                            </div>
                            <p>Thank you again for shopping with InfixVuci. <br>
                        Team InfixVuci.</p>
                        </div>
                    </div>
                    <div class="template_contact text-center">
                        <h4 class="font_24 f_w_700 mb_15" >Need Help?</h4>
                        <p class="mb_25 f_w_500">Happy to assist you. Our representatives are available from <br>
                        9:30am to 9:30pm, 7 days a week.</p>
                        <a href="#" class="theme_btn">Contact Us</a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="themplate_download_app text-center">
                    <h4 class="font_24 f_w_700 mb-0">Save More on App!</h4>
                    <p class="f_w_500">Get exclusive discounts, voucher & daily <br>
                        flash sales on the app.</p>
                    <div class="download_Links">
                        <a href="#">
                            <img src="img/template/google.svg" alt="">
                        </a>
                        <a href="#">
                            <img src="img/template/apple.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="template_footer">
                    <div class="template_footer_text">
                        <p>You have received this email because you or someone else has confirmed the email address. This would like 
                        to receive email communication from InfixVuci. We will never share your personal information 
                        (such as your email address with any other 3rd party without your consent).</p>
                        <p>This email was sent by: Tajwar Centre House No: 40 Baria Sreet 133/2 NY City, United States.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- template_signup_wrapper::end  -->


<?php include 'include/footer.php' ?>