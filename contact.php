<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Contact Us</h3>
                        <p><a href="index.php">Home </a>/ Contact Us</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- CONTACT::START -->
<div class="contact_section ">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 offset-xl-2">
                <div class="contact_map">
                    <div id="contact-map"></div>
                </div>
                <div class="contact_address">
                    <div class="row justify-content-center">
                        <div class="col-xl-10">
                            <div class="single_contact_address">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="contact_info ">
                                            <div class="contact_title mb_30">
                                                <span class="text-uppercase theme_text font_14 f_w_600 d-block">Contact </span>
                                                <h4 class="mb-0">Information</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="address_lines">
                                            <div class="single_address_line ">
                                                <span>Address <span>:</span> </span>
                                                <p>2593 Timbercrest Road, Chisana, Alaska 
                                                    Badalas United State.</p>
                                            </div>
                                            <div class="single_address_line">
                                                <span>Phone <span>:</span></span>
                                                <p>+61 (0) 3 8376 6284</p>
                                            </div>
                                            <div class="single_address_line">
                                                <span>E-mail<span>:</span></span>
                                                <p>noreply@codethemes.com</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="single_contact_address">
                                <div class="row">
                                    <div class="col-lg-5">
                                        <div class="contact_info ">
                                            <div class="contact_title mb_30">
                                                <span class="text-uppercase theme_text font_14 f_w_600 d-block">Send message </span>
                                                <h4 class="mb-0">Drop Us a Line</h4>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="contact_form_box">
                                            <form class="form-area contact-form" id="myForm" action="mail.php" method="post">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <input name="name" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'"
                                                            class="primary_line_input mb_10" required="" type="text">

                                                        <input name="email" placeholder="Type e-mail address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
                                                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail address'" class="primary_line_input mb_10"
                                                            required="" type="email">
                                                        <input name="subject" placeholder="Subject" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Subject'"
                                                            class="primary_line_input mb_10" required="" type="text">
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <textarea class="primary_line_textarea mb_30" name="message" placeholder="Message" onfocus="this.placeholder = ''"
                                                            onblur="this.placeholder = 'Message'" required=""></textarea>
                                                    </div>
                                                    <div class="col-lg-12 text-left">
                                                        <div class="alert-msg"></div>
                                                        <button type="submit" class="theme_btn small_btn submit-btn text-center" >Send Message</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- CONTACT::END -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>