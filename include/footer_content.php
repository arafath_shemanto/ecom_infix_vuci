    <!-- FOOTER::START  -->
    <footer>
        <div class="footer_top_area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="footer_product">
                            <div class="footer_product_left text-center">
                                <h4>Best Wireless Speaker</h4>
                                <span>For Perfect Home</span>
                            </div>
                            <div class="footer_product_right">
                                <h4>Apple MacBook Pro</h4>
                                <span>A touch of Genius</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main_footer_wrap">
            <div class="container">
                 <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-6">
                        <div class="footer_widget">
                            <div class="footer_logo">
                                <a href="#">
                                    <img src="img/logo2.png" alt="">
                                </a>
                            </div>
                            <div class="address_list">
                                <p> <span>Address:</span> No 40 Baria Sreet 133/2 NewYork 
                                City, Newyork, United States.</p>
                                <p><span>Call Us:</span> (+084) 123.456.789</p>
                                <p><span>Email:</span> contactinfo@InfixVuci.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>QUICK MENU</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="product.php">TV & Video</a></li>
                                <li><a href="product.php">Home Audio & Theatre</a></li>
                                <li><a href="product.php">Camera, Photo & Video</a></li>
                                <li><a href="product.php">Cell Phones & Accessories</a></li>
                                <li><a href="product.php">Headphones</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-3  col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>INFOMATION</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="about.php">About Us</a></li>
                                <li><a href="#">Brands</a></li>
                                <li><a href="#">Gift Vouchers</a></li>
                                <li><a href="#">Site Map</a></li>
                                <li><a href="#">Accessories</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-2  col-md-6">
                        <div class="footer_widget" >
                            <div class="footer_title">
                                <h3>MY ACCOUNT</h3>
                            </div>
                            <ul class="footer_links">
                                <li><a href="my_account.php">My Account</a></li>
                                <li><a href="my_order.php">Order History</a></li>
                                <li><a href="#">Wish List</a></li>
                                <li><a href="order_details.php">Order Details</a></li>
                                <li><a href="#">Wishlist</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright_area">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="copy_right_text">
                            <p>© 2021 <a href="#">InfixVuci</a>. All rights reserved. Made By <a href="https://codecanyon.net/user/codethemes/portfolio">CodeThemes</a>.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="payment_imgs ">
                            <img src="img/payment_img.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- FOOTER::END  -->

