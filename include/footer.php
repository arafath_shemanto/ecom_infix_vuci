
    <!-- shoping_cart::start  -->
    <div class="shoping_wrapper">
        <div class="dark_overlay"></div>
        <div class="shoping_cart">
            <div class="shoping_cart_inner">
                <div class="cart_header d-flex justify-content-between">
                    <h4>Shoping Cart</h4>
                    <div class="chart_close">
                        <i class="ti-close"></i>
                    </div>
                </div>
                <div class="single_cart">
                    <div class="thumb">
                        <img src="img/cart/1.png " alt="">
                    </div>
                    <div class="cart_content">
                        <a href="#">
                            <h5>The Unbundled University</h5>
                        </a>
                        <p>2 <span class="multy_icon" >x</span> <span class="prise" >$4200.00</span> </p>
                    </div>
                </div>
                <div class="single_cart">
                    <div class="thumb">
                        <img src="img/cart/1.png " alt="">
                    </div>
                    <div class="cart_content">
                        <a href="#">
                            <h5>The Unbundled University</h5>
                        </a>
                        <p>2 <span class="multy_icon" >x</span> <span class="prise" >$4200.00</span> </p>
                    </div>
                </div>
                <div class="single_cart">
                    <div class="thumb">
                        <img src="img/cart/1.png " alt="">
                    </div>
                    <div class="cart_content">
                        <a href="#">
                            <h5>The Unbundled University</h5>
                        </a>
                        <p>2 <span class="multy_icon" >x</span> <span class="prise" >$4200.00</span> </p>
                    </div>
                </div>
                <div class="single_cart">
                    <div class="thumb">
                        <img src="img/cart/1.png " alt="">
                    </div>
                    <div class="cart_content">
                        <a href="#">
                            <h5>The Unbundled University</h5>
                        </a>
                        <p>2 <span class="multy_icon" >x</span> <span class="prise" >$4200.00</span> </p>
                    </div>
                </div>
            </div>
            <div class="view_checkout_btn d-flex justify-content-end mb_30">
                <a href="cart.php" class="theme_btn small_btn3 mr_10">View cart</a>
                <a href="checkout.php" class="theme_btn small_btn3">Checkout</a>
            </div>
        </div>
    </div>
    <!-- shoping_cart::end  -->

    <!-- product_quick_view::strat  -->
    <!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#theme_modal">
  Launch demo modal
</button> -->

    <!-- Modal::start  -->
    <div class="modal fade theme_modal" id="theme_modal" tabindex="-1" role="dialog" aria-labelledby="theme_modal" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body p-0">
                    <div class="product_quick_view ">
                    <button type="button" class="close_modal_icon" data-dismiss="modal">
                        <i class="ti-close"></i>
                    </button>
                        <div class="product_details_img"></div>
                        <div class="product_details_wrapper">
                            <div class="product_content_details mb_30">
                                <span class="theme_btn green_btn cursor_default ">In Stock</span>
                                <h3>Wichita Hydraulic Table</h3>
                                <div class="product_ratings">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span> (6 customer reviews)</span>
                                </div>
                                <p class="details_text" >Ut ratione qui sunt officiis quo cum ut. Quas aliquam cum ipsam facerequaerat Et et aut quibusdam deleniti nisi sunt rerum. Consequatur molestiae address in molestias eius unde facere est architecto.</p>
                                <div class="product_info">
                                    <div class="single_pro_varient">
                                        <span class="font_14 f_w_600 theme_text3  text-uppercase" >Color:</span>
                                        <div class="color_List d-flex">
                                            <label class="round_checkbox blue_check  d-flex">
                                                <input name="color_filt" type="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="round_checkbox paste_check  d-flex">
                                                <input name="color_filt" type="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                            <label class="round_checkbox violate_check  d-flex">
                                                <input name="color_filt" type="radio">
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="single_pro_varient">
                                        <span class="font_14 f_w_600 theme_text3  text-uppercase" >Size    :</span>
                                        <ul class="product_size">
                                            <li>M</li>
                                            <li>L</li>
                                            <li>Xl</li>
                                        </ul>
                                    </div>
                                    <div class="single_pro_varient">
                                        <span class="font_14 f_w_600 theme_text3  text-uppercase" >QTY:</span>
                                        <div class="product_number_count" data-target="amount-1">
                                            <span class="count_single_item inumber_decrement"> <i class="ti-minus"></i></span>
                                            <input id="amount-1" class="count_single_item input-number" type="text" value="1">
                                            <span class="count_single_item number_increment"> <i class="ti-plus"></i></span>
                                        </div>
                                    </div>
                                    <div class="add_wishCard">
                                        <a href="#" data-dismiss="modal" class="theme_btn mr_15 add_to_cart">Add to Cart</a>
                                        <a class="add_wish_list" href="wishlist.php"> <i class="far fa-heart"></i> Add to Wishlist</a>
                                    </div>
                                </div>
                                <div class="pro_branding_info">
                                    <div class="single_branding">
                                        <span>Category<span>:</span></span>
                                        <p>Beds, Decoration</p>
                                    </div>
                                    <div class="single_branding">
                                        <span>SKU<span>:</span></span>
                                        <p>60983</p>
                                    </div>
                                    <div class="single_branding">
                                        <span>Brand <span>:</span></span>
                                        <p>Salink</p>
                                    </div>
                                    <div class="single_branding">
                                        <span>Share<span>:</span></span>
                                        <ul class="social_links">
                                            <li><a class="fab fa-facebook-f facebook_bg" href="#"></a></li>
                                            <li><a class="fab fa-twitter twitter_bg" href="#"></a></li>
                                            <li><a class="fab fa-linkedin-in linkedin_bg" href="#"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal::end  -->
    
    <!-- product_quick_view::end -->

    <!-- login_form:start -->
    <div class="modal fade login_modal" id="login_form" tabindex="-1" role="dialog" aria-labelledby="login_form" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div data-dismiss="modal" class="close_modal">
                        <i class="ti-close"></i>
                    </div>
                    <!-- infix_login_area::start  -->
                    <div class="infix_login_area p-0">
                        <div class="login_area_inner">
                            <h4>Welcome back, Please login
                                to your account </h4>
                            <div class="login_with_links">
                                <a href="#"> <i class="fab fa-google"></i> Login with google</a>
                                <a class="Facebook_bg" href="#"> <i class="ti-facebook"></i>Login with Facebook</a>
                                <a class="twitter_bg" href="#"> <i class="ti-twitter-alt"></i> Login with google</a>
                            </div>
                            <p class="sign_up_text mb_40">Or login with Email Address</p>
                            <form action="#">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="input-group custom_group_field mb_35">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <img src="img/my_account/email.svg" alt="">
                                                </span>
                                            </div>
                                            <input type="email" class="form-control" placeholder="E.g. example@gmail.com" aria-label="E.g. example@gmail.com" >
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-group custom_group_field ">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">
                                                    <img src="img/my_account/pass.svg" alt="">
                                                </span>
                                            </div>
                                            <input type="password" class="form-control" placeholder="Enter Password" aria-label="Enter Password" >
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="remember_pass mb_35">
                                            <label class="primary_checkbox d-flex">
                                                <input checked="" type="checkbox">
                                                <span class="checkmark mr_15"></span>
                                                <span class="label_name">Remember Me</span>
                                            </label>
                                            <a class="forgot_pass" href="#">Forgot Password</a>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button class="theme_btn w-100 text-center">Sign In</button>
                                    </div>
                                    <div class="col-12">
                                        <p class="sign_up_text" >Don’t have an account? <a data-toggle="modal" data-dismiss="modal" data-target="#resister_form" href="#">Sing Up</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- infix_login_area::end  -->
                </div>
            </div>
        </div>
    </div>
    <!-- login_form:end  -->

    <!-- resister_form:start  -->
    <div class="modal fade login_modal" id="resister_form" tabindex="-1" role="dialog" aria-labelledby="login_form" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body">
                <div data-dismiss="modal" class="close_modal">
                    <i class="ti-close"></i>
                </div>
                <!-- infix_login_area::start  -->
                    <div class="infix_login_area">
                        <div class="login_area_inner">
                            <h4>Welcome! Create an 
                                account within a minute.</h4>
                            <div class="login_with_links">
                                <a href="#"> <i class="fab fa-google"></i> Login with google</a>
                                <a class="Facebook_bg" href="#"> <i class="ti-facebook"></i>Login with Facebook</a>
                                <a class="twitter_bg" href="#"> <i class="ti-twitter-alt"></i> Login with google</a>
                            </div>
                            <p class="sign_up_text mb_20 pb-1">Or Register with Email Address</p>
                            <form action="#">
                                <div class="row">
                                    <div class="col-12">
                                        <input name="name" placeholder="Enter Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Name'"  class="primary_line_input mb_20" required="" type="text">

                                        <input name="email" placeholder="Type e-mail address" pattern="[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,63}$"
                                            onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail address'" class="primary_line_input mb_10" required="" type="email">
                                        <input name="password" placeholder="Enter password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter password'"
                                            class="primary_line_input mb_10" required="" type="password">
                                        <input name="password" placeholder="Re-enter password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Re-enter password'" class="primary_line_input mb_10" required="" type="password">
                                    </div>
                                    <div class="col-12">
                                        <div class="remember_pass mb_35 justify-content-start">
                                            <label class="primary_checkbox d-flex ">
                                                <input checked="" type="checkbox">
                                                <span class="checkmark mr_15"></span>
                                            </label>
                                            <p class="font_14 f_w_500 mb-0">By signing up, you agree to <a class="theme_text text_underline" href="#"> Terms of Service</a> and <a class="theme_text text_underline"  href="#">Privacy Policy.</a></p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <button class="theme_btn w-100 text-center">Sign Up</button>
                                    </div>
                                    <div class="col-12">
                                        <p class="sign_up_text" >Already have an account? <a data-toggle="modal" data-dismiss="modal" href="#" data-target="#login_form">login</a></p>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- infix_login_area::end  -->
                </div>
            </div>
        </div>
    </div>
    <!-- resister_form:end  -->
    
    <!-- UP_ICON  -->
    <div id="back-top" style="display: none;">
        <a title="Go to Top" href="#">
            <i class="ti-angle-up"></i>
        </a>
    </div>
    <!--/ UP_ICON -->

    <!--ALL JS SCRIPTS -->
    <script src="js/vendor/jquery-3.4.1.min.js"></script>
    <script src="js/vendor/popper.min.js"></script>
    <script src="js/vendor/bootstrap.min.js"></script>
    
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/isotope.pkgd.min.js"></script>
    <script src="js/waypoints.min.js"></script>
    <script src="js/jquery.counterup.min.js"></script>
    <script src="js/imagesloaded.pkgd.min.js"></script>
    <script src="js/wow.min.js"></script>
    <script src="js/nice-select.min.js"></script>
    <script src="js/barfiller.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/jquery.magnific-popup.min.js"></script>
    <script src="js/jquery.ajaxchimp.min.js"></script>
    <script src="js/parallax.js"></script>
     <script src="js/gijgo.min.js"></script>
     <!-- query-ui -->
     <?php if (basename($_SERVER['PHP_SELF']) == 'product.php') {?>
        <script src="js/query-ui.js"></script>
    <?php }?>
    <?php if (basename($_SERVER['PHP_SELF']) == 'product2.php') {?>
        <script src="js/query-ui.js"></script>
    <?php }?>
    <?php if (basename($_SERVER['PHP_SELF']) == 'product3.php') {?>
        <script src="js/query-ui.js"></script>
    <?php }?>
    
    <script src="js/jquery.countdown.min.js"></script>
    <script src="js/mail-script.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7nx22ZmINYk9TGiXDEXGVxghC43Ox6qA"></script>
    <script src="js/map.js"></script>
    <!-- MAIN JS   -->
    <script src="js/main.js"></script>

</body>

</html>