    <!-- HEADER::START -->
    <header>
        <div id="sticky-header" class="header_area">
            <div class="header_top_area">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <div class="header__wrapper">
                                <!-- header__left__start  -->
                                <div class="header__left d-flex align-items-center">
                                    <div class="logo_img">
                                        <a href="index.php">
                                            <img src="img/logo.png" alt="">
                                        </a>
                                    </div>
                                </div>
                                <!-- header__left__start  -->

                                <!-- header__right_start  -->
                                <div class="header__right d-flex">
                                    <div class="category_search d-flex">
                                        <!-- phone_number  -->
                                        <div class="phone_number d-flex align-item-center">
                                            <div class="icon">
                                                <img src="img/svg/headset.svg" alt="">
                                            </div>
                                            <div class="number_content">
                                                <span>CALL US</span>
                                                <h4>+88 0184 1136251</h4>
                                            </div>
                                        </div>
                                        <div class="input-group theme_search_field ">
                                            <div class="input-group-prepend">
                                                <button class="btn" type="button" id="button-addon1"> <i class="ti-search"></i> </button>
                                            </div>
                                            <input type="text" class="form-control" placeholder="Search for course, skills and Videos">
                                        </div>
                                    </div>
                                    <div class="notification_wrapper d-flex align-items-center">
                                        <a href="#" data-toggle="modal" data-target="#login_form" class="single_notify">
                                            <img src="img/svg/man.svg" alt="">
                                        </a>
                                        <a href="wishlist.php" class="single_notify notify_wish">
                                            <img src="img/svg/hert.svg" alt="">
                                            <span class="notify_count">2</span>
                                        </a>
                                        <a href="cart.php" class="single_notify">
                                            <img src="img/svg/cart.svg" alt="">
                                            <span class="notify_count">2</span>
                                        </a>
                                    </div>
                                </div>
                                <!-- header__right_end  -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- main_header_area  -->
            <div class="main_header_area">
                <div class="container">
                    <div class="row">
                         <div class="col-xl-11">
                             <div class="shop_header_wrapper d-flex align-items-center">
                                <div class="menu_logo">
                                        <a href="index.php">
                                            <img src="img/logo.png" alt="">
                                        </a>
                                    </div>
                                 <div class="dropdown show category_menu">
                                    <a class="Categories_togler">
                                        All Categories 
                                        <i class="fas fa-chevron-down"></i>
                                    </a>
                                    <ul class="dropdown_menu catdropdown_menu">
                                        <li><a class="dropdown-item has_arrow"><i class="ti-desktop"></i> Laptops & Computers</a>
                                            <!-- 2nd level  --> 
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow" href="#"><i class="ti-camera"></i> Home Enternteinment</a>
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow" href="#"><i class="ti-game"></i>  Music Headphones</a>
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow" href="#"><i class="ti-brush-alt"></i>  Music Headphones</a>
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow"><i class="ti-desktop"></i> Laptops & Computers</a>
                                            <!-- 2nd level  --> 
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow" href="#"><i class="ti-camera"></i> Home Enternteinment</a>
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow" href="#"><i class="ti-game"></i>  Music Headphones</a>
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow" href="#"><i class="ti-brush-alt"></i>  Music Headphones</a>
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow" href="#"><i class="ti-brush-alt"></i>  Music Headphones</a>
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow"><i class="ti-desktop"></i> Laptops & Computers</a>
                                            <!-- 2nd level  --> 
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                        <li><a class="dropdown-item has_arrow"><i class="ti-desktop"></i> Laptops & Computers</a>
                                            <!-- 2nd level  --> 
                                            <ul class="submenu_list" >
                                                <li><a class="dropdown-item" href="product.php">Computer Components</a></li>
                                                <li><a class="dropdown-item" href="product.php">Networking</a></li>
                                                <li><a class="dropdown-item" href="product.php">Computer Monitors</a></li>
                                                <li><a class="dropdown-item" href="product.php">Desktop Computers</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                                 <!-- main_menu_start  -->
                                <div class="main_menu  d-none d-lg-block">
                                    <nav>
                                        <ul id="mobile-menu">
                                            <li><a href="index.php">Home</a></li>
                                            <li><a href="product.php">New Arrivals</a>
                                                <!-- mega_width_menu  -->
                                                <ul class="submenu mega_width_menu">
                                                    <!-- single_menu  -->
                                                    <li>
                                                        <a class="mega_metu_title" href="#">Laptops & Computers</a>
                                                        <ul>
                                                            <li><a href="product.php">Desktop Computers</a></li>
                                                            <li><a href="product.php">Computer Accessories</a></li>
                                                            <li><a href="product.php">Computer Components</a></li>
                                                            <li><a href="product.php">Networking</a></li>
                                                            <li><a href="product.php">Computer Monitors</a></li>
                                                        </ul>
                                                    </li>
                                                    <!-- single_menu  -->
                                                    <li>
                                                        <a class="mega_metu_title" href="#">Home Enternteinment</a>
                                                        <ul>
                                                            <li><a href="product.php">Desktop Computers</a></li>
                                                            <li><a href="product.php">Computer Accessories</a></li>
                                                            <li><a href="product.php">Computer Components</a></li>
                                                            <li><a href="product.php">Networking</a></li>
                                                            <li><a href="product.php">Computer Monitors</a></li>
                                                        </ul>
                                                    </li>
                                                    <!-- single_menu  -->
                                                    <li>
                                                        <a class="mega_metu_title" href="#">Music Headphones</a>
                                                        <ul>
                                                            <li><a href="product.php">Desktop Computers</a></li>
                                                            <li><a href="product.php">Computer Accessories</a></li>
                                                            <li><a href="product.php">Computer Components</a></li>
                                                            <li><a href="product.php">Networking</a></li>
                                                            <li><a href="product.php">Computer Monitors</a></li>
                                                        </ul>
                                                    </li>
                                                    <!-- single_menu  -->
                                                    <li>
                                                        <a class="mega_metu_title" href="#">Music Headphones</a>
                                                        <ul>
                                                            <li><a href="product.php">Desktop Computers</a></li>
                                                            <li><a href="product.php">Computer Accessories</a></li>
                                                            <li><a href="product.php">Computer Components</a></li>
                                                            <li><a href="product.php">Networking</a></li>
                                                            <li><a href="product.php">Computer Monitors</a></li>
                                                        </ul>
                                                    </li>
                                                    <!-- single_menu  -->
                                                    <li class="img_menu" >
                                                        <ul>
                                                            <li>
                                                                <h6>PREMIUM SUPPLEMENTS</h6>
                                                            </li>
                                                            <li>
                                                            <h4>Wooden Minimalistic <br>
                                                                Chairs for Home.</h4>
                                                            </li>
                                                            <li>
                                                                <a class="shop_now" href="product.php">Shop Now »</a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                                <!--/ mega_width_menu -->
                                            </li>
                                            <li><a href="product2.php">Trending</a></li>
                                            <li><a href="product3.php"> Hot Deals</a></li>
                                            <li class="submenu_active"><a href="#">Pages <i class="ti-angle-down"></i> </a>
                                                <ul class=" submenu">
                                                    <li><a href="about.php">About</a></li>
                                                    <li><a href="error_404.php">Error Page</a></li>
                                                    <li><a href="career.php">Career</a></li>
                                                    <li><a href="login.php">login</a></li>
                                                    <li><a href="faq.php">Faq</a></li>
                                                    <li><a href="cart.php">cart</a></li>
                                                    <li><a href="blog.php">blog</a></li>
                                                    <li><a href="my_account.php">My Account</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="contact.php">Contact</a></li>
                                        </ul>
                                    </nav>
                                </div>
                                <!-- main_menu_start  -->
                                <div class="offer_tag d-flex align-items-center">
                                    <img src="img/svg/offer.svg" alt="">
                                    <p>Clearance <a> Up to 30% Off</a></p>
                                </div>
                                <div class="input-group theme_search_field ">
                                    <div class="input-group-prepend">
                                        <button class="btn" type="button" > <i class="ti-search"></i> </button>
                                    </div>
                                    <input type="text" class="form-control" placeholder="Search for course, skills and Videos">
                                </div>
                             </div>
                         </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--/ HEADER::END -->