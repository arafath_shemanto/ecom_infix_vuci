<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Career</h3>
                        <p><a href="index.php">Home </a>/ career</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- career_extraordinary_area::start  -->
<div class="career_extraordinary_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="section__title3 text-center">
                    <h3>Come Do The Extraordinary</h3>
                    <p class="mb-0">Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Sed ut perspiciatis.Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eu fugiat nulla pariatur.</p>
                </div>
                <div class="career_extraordinary_banner">
                    <img src="img/career/banner.jpg" alt="">
                </div>
                <div class="career_extraordinary_content">
                    <div class="row align-items-center">
                        <div class="col-lg-7 mb_30">
                            <h4>Company Value</h4>
                            <p>Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div class="col-lg-5 text-right">
                            <a href="#available_position" class="theme_btn mb_30">See Available Positions</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- career_extraordinary_area::end  -->

<!-- career_wrapper::start  -->
<div class="career_wrapper">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-lg-6">
                <div class="career_banner">
                    <img src="img/career/1.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="career_info">
                    <div class="career_info_inner">
                        <h3 >Company Culture</h3>
                        <p class="mb_25">Konseptbizden believes everyone should live in a home they love. Through technology and innovation, Konseptbizden makes it possible for shoppers to quickly and easily find exactly what they want from a selection of more than 18 million items across home furnishings.</p>
                        <p>Committed to delighting its customers every step of the way, Konseptbizden is reinventing the way people shop for their homes - from product discovery to final delivery Housewares and more.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters tab_reverse">
        <div class="col-lg-6">
                <div class="career_info">
                    <div class="career_info_inner">
                        <h3>Benefits We Offers</h3>
                        <p class="mb_20">Konseptbizden believes everyone should live in a home they love. Through technology and innovation makes it possible.</p>
                        <ul class="circle_lists">
                            <li> <span></span> Open vacation policy for salary employees.</li>
                            <li> <span></span> Paid volunteer time.</li>
                            <li> <span></span> 15 days PTO + paid sick time for hourly employees.</li>
                            <li> <span></span> Adoption assistance.</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="career_banner">
                    <img src="img/career/2.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- career_wrapper::end  -->

<!-- career_available_area::start  -->
<div class="career_available_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title3 mb_53 text-center">
                    <h3 class="mb-0">We Are Available On</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-3 col-lg-3 col-md-4">
                <div class="single_career_available mb_30">
                    <div class="thumb">
                        <img src="img/career/available_1.jpg" alt="">
                    </div>
                    <div class="career_available_content text-center">
                        <h4>Germany</h4>
                        <p>2593 Timbercrest Road, Chisana, Alaska 
                        Badalas United State.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4">
                <div class="single_career_available mb_30">
                    <div class="thumb">
                        <img src="img/career/available_2.jpg" alt="">
                    </div>
                    <div class="career_available_content text-center">
                        <h4>Canada</h4>
                        <p>2593 Timbercrest Road, Chisana, Alaska 
                        Badalas United State.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4">
                <div class="single_career_available mb_30">
                    <div class="thumb">
                        <img src="img/career/available_3.jpg" alt="">
                    </div>
                    <div class="career_available_content text-center">
                        <h4>Italy</h4>
                        <p>2593 Timbercrest Road, Chisana, Alaska 
                        Badalas United State.</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-lg-3 col-md-4">
                <div class="single_career_available mb_30">
                    <div class="thumb">
                        <img src="img/career/available_4.jpg" alt="">
                    </div>
                    <div class="career_available_content text-center">
                        <h4>Brazil</h4>
                        <p>2593 Timbercrest Road, Chisana, Alaska 
                        Badalas United State.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- career_available_area::end  -->

<!-- availabale_position_area::start  -->
<div id="available_position" class="availabale_position_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section__title3 mb_50 text-center">
                    <h3 class="mb-0">Available Positions</h3>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="table-responsive">
                    <table class="table position_table">
                        <thead>
                            <tr>
                            <th scope="col">
                                <span class="job_lists" >Job List</span>
                            </th>
                            <th></th>
                            <th></th>
                            <th class="d-flex justify-content-end" scope="col">
                                <select class="theme_select2 ml-auto mr-0" >
                                    <option data-display="Show All">Show All</option>
                                    <option value="1">Show A-Z</option>
                                    <option value="1">Show 1-10</option>
                                </select>
                            </th>
                            </tr>
                        </thead>    
                        <tbody>
                            <tr>
                                <td>
                                    <a href="career_details.php"><h5 class="job_name">UI/UX Designer</h5></a>
                                    <span class="job_position">Design</span>
                                </td>
                                <td>
                                    <h5 class="job_name">3 - 6 Years</h5>
                                    <span class="job_position">Experience</span>
                                </td>
                                <td>
                                    <h5 class="job_name">Job Type</h5>
                                    <span class="job_position">Part - Time</span>
                                </td>
                                <td>
                                    <h5 class="job_name">Deadline</h5>
                                    <span class="job_position">December 31, 2020</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="career_details.php"><h5 class="job_name">UI/UX Designer</h5></a>
                                    <span class="job_position">Design</span>
                                </td>
                                <td>
                                    <h5 class="job_name">3 - 6 Years</h5>
                                    <span class="job_position">Experience</span>
                                </td>
                                <td>
                                    <h5 class="job_name">Job Type</h5>
                                    <span class="job_position">Part - Time</span>
                                </td>
                                <td>
                                    <h5 class="job_name">Deadline</h5>
                                    <span class="job_position">December 31, 2020</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="career_details.php"><h5 class="job_name">UI/UX Designer</h5></a>
                                    <span class="job_position">Design</span>
                                </td>
                                <td>
                                    <h5 class="job_name">3 - 6 Years</h5>
                                    <span class="job_position">Experience</span>
                                </td>
                                <td>
                                    <h5 class="job_name">Job Type</h5>
                                    <span class="job_position">Part - Time</span>
                                </td>
                                <td>
                                    <h5 class="job_name">Deadline</h5>
                                    <span class="job_position">December 31, 2020</span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a href="career_details.php"><h5 class="job_name">UI/UX Designer</h5></a>
                                    <span class="job_position">Design</span>
                                </td>
                                <td>
                                    <h5 class="job_name">3 - 6 Years</h5>
                                    <span class="job_position">Experience</span>
                                </td>
                                <td>
                                    <h5 class="job_name">Job Type</h5>
                                    <span class="job_position">Part - Time</span>
                                </td>
                                <td>
                                    <h5 class="job_name">Deadline</h5>
                                    <span class="job_position">December 31, 2020</span>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- availabale_position_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>