<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Update Account Info</h3>
                        <p><a href="index.php">Home </a>/ My Account / Update account info</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- account_info_area::start  -->
<div class="account_info_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-9 offset-xl-1">
                <div class="account_profile_wrapper">
                    <div class="account_profile_thumb text-center mb_30">
                        <div class="thumb">
                            <img src="img/my_account/profile.png" alt="">
                        </div>
                        <h4>Robert Downey JR.</h4>
                        <p>UX/UI Designer</p>
                    </div>
                    <div class="account_profile_form">
                        <div class="account_title">
                            <h3 class="font_30 f_w_700 ">Account Details</h3>
                            <p class="mb-0 font_1 f_w_400 theme_text1" >Edit your account settings and change your password here.</p>
                        </div>
                        <form action="#">
                            <div class="row">
                                <div class="col-lg-12">
                                    <label class="primary_label mb_20_imporatnt">First Name</label>
                                    <input name="current_pass" placeholder="Robert Downey" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Robert Downey'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12">
                                    <input name="current_pass" placeholder="Junior" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Junior'" class="primary_input3" required="" type="text">
                                </div>
                                <div class="col-12">
                                    <div class="theme_border"></div>
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label mb_20_imporatnt">Email Address</label>
                                        <input name="email" placeholder="Your email address is spn12@spondonit.com" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your email address is spn12@spondonit.com'" class="primary_input3" required="" type="email">
                                </div>
                                <div class="col-12">
                                    <div class="theme_border"></div>
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label mb_20_imporatnt">Current Password</label>
                                    <input name="current_pass" placeholder="Enter your current password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your current password'" class="primary_input3 mb_15" required="" type="password">
                                    <input name="pass" placeholder="Enter your new password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter your new password'" class="primary_input3 mb_15" required="" type="password">
                                    <input name="pass" placeholder="Re-type your new password" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Re-type your new password'" class="primary_input3 mb_30" required="" type="password">
                                </div>
                                <div class="col-12">
                                    <button class="theme_btn">Update Information</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- account_info_area::end  -->


<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>