<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- banner::start  -->
<div class="banner_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="big_banner">
                    <div class="banner_text">
                        <span>Deals and promotions</span>
                        <h3>Keep your feet <br> Cool comfy.</h3>
                        <div class="product_prise">
                            <span>$325.00</span>
                            <h4>$299 <span>.99</span> </h4>
                        </div>
                        <a href="product.php" class="theme_btn">Shop Now</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="row">
                    <div class="col-12">
                        <div class="small_banner mb_30">
                            <div class="small_banner_text">
                                <h4>Smart Speaker For <br>
                                    Music Lovers</h4>
                                <div class="prise_tag">
                                    From <span>$39 <span>.99</span> </span>
                                </div>
                                <a href="product.php" class="line_btn">Shop Now »</a>
                            </div>
                            <div class="small_img">
                                <img src="img/banner/small_img_1.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="small_banner small_banner2">
                            <div class="small_banner_text">
                                <h4>Smart Speaker For <br>
                                    Music Lovers</h4>
                                <div class="prise_tag">
                                    From <span>$39 <span>.99</span> </span>
                                </div>
                                <a href="product.php" class="line_btn">Shop Now »</a>
                            </div>
                            <div class="small_img mr_10">
                                <img src="img/banner/small_img_2.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ banner::end  -->

<!-- popular_category::start  -->
<div class="popular_category">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main_title text-center mb_60">
                    <h3 class="mb-0" >Explore Popular Categories</h3>
                </div>
            </div>
            <div class="col-12">
                <div class="carousel_active owl-carousel">
                    <!-- single_carousel  -->
                    <div class="single_carousel text-center">
                        <div class="thumb">
                            <img src="img/category/cat1.png" alt="">
                        </div>
                        <a href="product.php">
                            <h4 class="font_18 f_w_700">
                                Smart Phones
                            </h4>
                        </a>
                        <p class="mute_text font_14" >24 Products</p>
                    </div>
                    <!-- single_carousel  -->
                    <div class="single_carousel text-center">
                        <div class="thumb">
                            <img src="img/category/cat2.png" alt="">
                        </div>
                        <a href="product.php">
                            <h4 class="font_18 f_w_700">
                            Digital Cameras
                            </h4>
                        </a>
                        <p class="mute_text font_14" >24 Products</p>
                    </div>
                    <!-- single_carousel  -->
                    <div class="single_carousel text-center">
                        <div class="thumb">
                            <img src="img/category/cat3.png" alt="">
                        </div>
                        <a href="product.php">
                            <h4 class="font_18 f_w_700">
                            Televisions
                            </h4>
                        </a>
                        <p class="mute_text font_14" >24 Products</p>
                    </div>
                    <!-- single_carousel  -->
                    <div class="single_carousel text-center">
                        <div class="thumb">
                            <img src="img/category/cat4.png" alt="">
                        </div>
                        <a href="product.php">
                            <h4 class="font_18 f_w_700">
                            Audio Player
                            </h4>
                        </a>
                        <p class="mute_text font_14" >24 Products</p>
                    </div>
                    <!-- single_carousel  -->
                    <div class="single_carousel text-center">
                        <div class="thumb">
                            <img src="img/category/cat5.png" alt="">
                        </div>
                        <a href="product.php">
                            <h4 class="font_18 f_w_700">
                            Computer Assets
                            </h4>
                        </a>
                        <p class="mute_text font_14" >24 Products</p>
                    </div>
                    <!-- single_carousel  -->
                    <div class="single_carousel text-center">
                        <div class="thumb">
                            <img src="img/category/cat6.png" alt="">
                        </div>
                        <a href="product.php">
                            <h4 class="font_18 f_w_700">
                            Smart Watchs
                            </h4>
                        </a>
                        <p class="mute_text font_14" >24 Products</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/ popular_category::end  -->

<!-- new_arrivales_area::start  -->
<div class="new_arrivales_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="arrival_menu">
                    <div class="main_title mb_30">
                        <h3 class="mb-0" >New Arrivals</h3>
                    </div>
                    <ul class="nav custom_nav" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="Footwear-tab" data-toggle="tab" href="#Footwear" role="tab" aria-controls="Footwear" aria-selected="true">Footwear                                         </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Apparel-tab" data-toggle="tab" href="#Apparel" role="tab" aria-controls="Apparel" aria-selected="false">Apparel</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Outdoor-tab" data-toggle="tab" href="#Outdoor" role="tab" aria-controls="Outdoor" aria-selected="false">Outdoor</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="Accessories-tab" data-toggle="tab" href="#Accessories" role="tab" aria-controls="Accessories" aria-selected="false">Accessories</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="Footwear" role="tabpanel" aria-labelledby="Footwear-tab">
                        <!-- content  -->
                        <div class="product_slide product_borderLess owl-carousel">
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/1.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/2.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/3.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/4.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/1.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                        </div>
                        <!--/ content  -->
                    </div>
                    <div class="tab-pane fade" id="Apparel" role="tabpanel" aria-labelledby="Apparel-tab">
                        <!-- content  -->
                        <div class="product_slide product_borderLess owl-carousel">
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/1.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/2.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/3.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/4.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/1.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                        </div>
                        <!--/ content  -->
                    </div>
                    <div class="tab-pane fade" id="Outdoor" role="tabpanel" aria-labelledby="Outdoor-tab">
                        <!-- content  -->
                        <div class="product_slide product_borderLess owl-carousel">
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/1.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/2.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/3.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/4.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/1.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                        </div>
                        <!--/ content  -->
                    </div>
                    <div class="tab-pane fade" id="Accessories" role="tabpanel" aria-labelledby="Accessories-tab">
                        <!-- content  -->
                        <div class="product_slide product_borderLess owl-carousel">
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/1.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/2.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/3.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/4.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                            <!-- product_wized  -->
                            <div class="product_wized mb_30">
                                <div class="thumb">
                                    <img src="img/product/1.png" alt="">
                                    <div class="product_action">
                                        <div class="product_action_inner">
                                            <a href="#">
                                                <i class="ti-heart"></i>
                                            </a>
                                            <a href="#">
                                                <i class="ti-control-shuffle"></i>
                                            </a>
                                            <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product_wized_body">
                                    <div class="stars">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                    <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                    <a href="product_details.php">
                                        <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                    </a>
                                    <p class="font_16  theme_text f_w_500">$180.00</p>
                                </div>
                                <a href="#" class="add_to_cart">Add to Cart</a>
                            </div>
                        </div>
                        <!--/ content  -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- new_arrivales_area::end  -->

<!-- deal_day_wrapper::start  -->
<div class="deal_day_wrapper">
    <div class="deal_day_content">
        <span class="f_w_600 font_14 theme_text text-uppercase" >Deal of the Day</span>
        <h3>Home Smart Speaker with 
        Google Assistant.</h3>
        <div class="product_prise">
            <span>$325.00</span>
            <h4>$299 <span>.99</span> </h4>
        </div>
        <h4 class="end_text font_14 f_w_600 theme_text text-uppercase">Deal ends in</h4>
        <div id="count_down" class="deals_end_count">
        </div>
    </div>
</div>
<!-- deal_day_wrapper::end  -->

<!-- Featured_Products_area::start  -->
<div class="Featured_Products_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main_title mb_30">
                    <h3 class="mb-0">Featured Products</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="product_slide product_borderLess owl-carousel">
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/5.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/6.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/7.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/8.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/6.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Featured_Products_area::end  -->

<!-- full_banenr::start  -->
<div class="full_banenr">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="full_banner_inner">
                    <div class="full_banner_text">
                        <span class="font_14 theme_text text-uppercase f_w_600"  >Sofa Furniture</span>
                        <h4>Wooden Minimalistic
                            Chairs for Home.</h4>
                        <span class="font_14 theme_text3  text-uppercase f_w_600" >SALE UP TO <span class="theme_text" >40%</span> OFF</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- full_banenr::end -->

<!-- processing_wrapper::start  -->
<div class="processing_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-md-6">
                <div class="single_processing">
                    <div class="icon">
                        <img src="img/svg/delevary.png" alt="">
                    </div>
                    <div class="processign_content">
                        <h4>Free Delivery</h4>
                        <p>Free Shipping on Order $500+</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="single_processing">
                    <div class="icon">
                        <img src="img/svg/clock.png" alt="">
                    </div>
                    <div class="processign_content">
                        <h4>90 DAYS RETURN</h4>
                        <p>Change of mind is not applicable</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="single_processing">
                    <div class="icon">
                        <img src="img/svg/payment.png" alt="">
                    </div>
                    <div class="processign_content">
                        <h4>SECURE PAYMENT</h4>
                        <p>World’s secure payment methods</p>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-md-6">
                <div class="single_processing">
                    <div class="icon">
                        <img src="img/svg/help.png" alt="">
                    </div>
                    <div class="processign_content">
                        <h4>24/7 Help Center</h4>
                        <p>Round-the-clock assistance Helpline</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- processing_wrapper::end  -->

<!-- topSelling_Products_area::start  -->
<div class="topSelling_Products_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main_title mb_30">
                    <h3 class="mb-0">Top Selling Products</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="product_slide product_borderLess owl-carousel">
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/9.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/10.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/11.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/12.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                    <div class="product_wized mb_30">
                        <div class="thumb">
                            <img src="img/product/10.jpg" alt="">
                            <div class="product_action">
                                <div class="product_action_inner">
                                    <a href="#">
                                        <i class="ti-heart"></i>
                                    </a>
                                    <a href="#">
                                        <i class="ti-control-shuffle"></i>
                                    </a>
                                    <a data-toggle="modal" data-target="#theme_modal">
                                        <i class="ti-eye"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="product_wized_body">
                            <div class="stars">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                            </div>
                            <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                            <a href="product_details.php">
                                <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                            </a>
                            <p class="font_16  theme_text f_w_500">$180.00</p>
                        </div>
                        <a href="#" class="add_to_cart">Add to Cart</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- topSelling_Products_area::end  -->

<!-- brand_area::start  -->
<div class="brand_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main_title mb_45">
                    <h3 class="mb-0">Top Brands</h3>
                    <div class="dash_border"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="brand_active owl-carousel">
                    <div class="single_brand">
                        <img src="img/brand/1.png" alt="">
                    </div>
                    <div class="single_brand">
                        <img src="img/brand/2.png" alt="">
                    </div>
                    <div class="single_brand">
                        <img src="img/brand/3.png" alt="">
                    </div>
                    <div class="single_brand">
                        <img src="img/brand/4.png" alt="">
                    </div>
                    <div class="single_brand">
                        <img src="img/brand/5.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="border_bottom_1px"></div>
    </div>
</div>
<!-- brand_area::end  -->

<!-- blog_area::start  -->
<div class="blog_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main_title mb_30">
                    <h3 class="mb-0">Form Our Blog</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/1.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>Thinking beyond influent social 
                                fashion style icon.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/2.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>The Complex Legacy of Vigilantism 
                            in South Africa.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/3.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>What It Would Take for Evangelicals 
                            to Turn on Trump.</h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

    <!-- newsletter_form ::start  -->
    <div class="newsletter_form_wrapper">
        <div class="newsletter_form_inner">
            <div class="close_modal">
                <i class="ti-close"></i>
            </div>
            <div class="newsletter_form_thumb"></div>
            <div class="newsletter_form text-center">
                <h3>Newsletter</h3>
                <p>Join over 1,000 people who get free & fresh content 
                delivered each time we publish.</p>
                <form action="#">
                    <div class="row">
                        <div class="col-12">
                            <input placeholder="Enter e-mail address" class="primary_input3 mb_10" type="text">
                        </div>
                        <div class="col-12">
                            <button class="theme_btn w-100 text-center">Subscribe Now</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- newsletter_form ::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>