<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Apply For Job</h3>
                        <p><a href="index.php">Home </a>/ career / Apply for job</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- apply_job_area::start  -->
<div class="apply_job_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="row">
                    <div class="col-12">
                        <!-- Information  -->
                        <div class="single_apply_wrapper mb_30">
                            <h3 class="font_30 f_w_700 mb_45">Personal Information</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="primary_label">First Name *</label>
                                        <input class="primary_input3 mb_18" placeholder="First Name" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Last Name *</label>
                                        <input class="primary_input3 mb_18" placeholder="Last Name" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Email Address *</label>
                                        <input class="primary_input3 mb_18" placeholder="Email Address" type="email">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Confirm Email Address *</label>
                                        <input class="primary_input3 mb_18" placeholder="Confirm Email Address" type="email">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Location (City) *</label>
                                        <input class="primary_input3 mb_18" placeholder="Location (City) *" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Phone No *</label>
                                        <input class="primary_input3 mb_18" placeholder="Phone No *" type="text">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="theme_border"></div>
                        <!-- Experience  -->
                        <div class="single_apply_wrapper add_collaspe_wrapper">
                            <div class="add_collaspe_header">
                                <h3 class="font_30 f_w_700">Experience</h3>
                                <button class="theme_btn raduis_30px width_210px add_collaspe_btn">+ Add Experiences</button>
                            </div>
                            <form class="collaspe_form" action="#">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="primary_label">Job Title *</label>
                                        <input class="primary_input3 mb_18" placeholder="Job Title *" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Company</label>
                                        <input class="primary_input3 mb_18" placeholder="Company" type="text">
                                    </div>
                                    <div class="col-md-12">
                                        <label class="primary_label">Location (City)</label>
                                        <input class="primary_input3 mb_18" placeholder="Location (City)" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Form *</label>
                                        <input class="primary_input3 mb_18" id="start_datepicker" placeholder="Pick a date" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">To *</label>
                                        <input class="primary_input3 mb_18"  id="end_datepicker" placeholder="Pick a date" type="text">
                                    </div>
                                    <div class="col-12 text-right mb_50 d-flex justify-content-end">
                                        <span class="theme_btn2 width_120px mr_10 hide_collape_form">Cancel</span>
                                        <button class="theme_btn width_120px">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="theme_border"></div>
                        <div class="single_apply_wrapper add_collaspe_wrapper">
                            <div class="add_collaspe_header">
                                <h3 class="font_30 f_w_700">Educations</h3>
                                <button class="theme_btn raduis_30px width_210px add_collaspe_btn">+ Add Education</button>
                            </div>
                            <form class="collaspe_form"  action="#">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="primary_label">Institution *</label>
                                        <input class="primary_input3 mb_18" placeholder="Institution *" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Degree</label>
                                        <input class="primary_input3 mb_18" placeholder="Degree" type="text">
                                    </div>
                                    <div class="col-md-12">
                                        <label class="primary_label">Location (City)</label>
                                        <input class="primary_input3 mb_18" placeholder="Location (City)" type="text">
                                    </div>
                                    <div class="col-md-12">
                                        <label class="primary_label">Description</label>
                                        <input class="primary_input3 mb_18" placeholder="Description" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Form *</label>
                                        <input class="primary_input3 mb_18" id="start_datepicker2" placeholder="Pick a date" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">To *</label>
                                        <input class="primary_input3 mb_18"  id="end_datepicker2" placeholder="Pick a date" type="text">
                                    </div>
                                    <div class="col-12 text-right mb_50 d-flex justify-content-end">
                                        <span class="theme_btn2 width_120px mr_10 hide_collape_form">Cancel</span>
                                        <button class="theme_btn width_120px">Save</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="theme_border mb_40"></div>
                        <div class="single_apply_wrapper">
                            <h3 class="font_30 f_w_700 mb_45">On the Web</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label class="primary_label">Facebook</label>
                                        <input class="primary_input3 mb_18" placeholder="Facebook" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Twitter</label>
                                        <input class="primary_input3 mb_18" placeholder="Twitter" type="email">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Linkedin</label>
                                        <input class="primary_input3 mb_18" placeholder="Linkedin" type="text">
                                    </div>
                                    <div class="col-md-6">
                                        <label class="primary_label">Website</label>
                                        <input class="primary_input3 mb_18" placeholder="Website" type="text">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="theme_border mb_30 mt_30"></div>
                        <div class="single_apply_wrapper ">
                            <h3 class="font_30 f_w_700 mb_30">Resume</h3>
                            <form action="#">
                                <div class="row">
                                    <div class="col-12 mb_50">
                                        <div class="file_upload">
                                            <div class="file_upload_inner text-center">
                                                <div class="thumb">
                                                    <img src="img/upload.svg" alt="">
                                                </div>
                                                <p>Drop your file here</p>
                                                <span>or choose files to upload <input type="file"> </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="theme_border mb_40"></div>
                                    </div>
                                    <div class="col-md-12 mb_30">
                                        <label class="primary_label">Message to Hiring Manager</label>
                                        <textarea class="primary_textarea3" placeholder="Let the company know about your interest working there" ></textarea>
                                    </div>
                                    <div class="col-12">
                                        <button class="theme_btn w-100 text-center large_btn">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- apply_job_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>