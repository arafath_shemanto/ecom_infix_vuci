<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_1">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>New Arrivals</h3>
                        <p><a href="index.php">Home </a>/ New Arrivals</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- prodcuts_area ::start  -->
<div class="prodcuts_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-xl-3">
                <div class="product_category_chose mb_50 mt_10">
                    <div class="course_title mb_30 d-flex align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="19.5" height="13" viewBox="0 0 19.5 13">
                            <g id="filter-icon" transform="translate(28)">
                                <rect id="Rectangle_1" data-name="Rectangle 1" width="19.5" height="2" rx="1" transform="translate(-28)" fill="#fd1c5e"/>
                                <rect id="Rectangle_2" data-name="Rectangle 2" width="15.5" height="2" rx="1" transform="translate(-26 5.5)" fill="#fd1c5e"/>
                                <rect id="Rectangle_3" data-name="Rectangle 3" width="5" height="2" rx="1" transform="translate(-20.75 11)" fill="#fd1c5e"/>
                            </g>
                        </svg>
                        <h5 class="font_16 f_w_600 mb-0">Filter Category</h5>
                    </div>
                    <div class="course_category_inner">
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_600">
                                Product categories
                            </h4>
                            <ul class="Check_sidebar mb_35">
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Beauty & Health</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Clothing</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Electronics & Computers</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Food & Grocery</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Furniture</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Garden & Kitchen</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                            Filter by Brands
                            </h4>
                            <ul class="Check_sidebar mb_35">
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">La Re Ve</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Easy Fashion</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Yellow</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Estasy</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Unileaver</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                            Filter by Size
                            </h4>
                            <ul class="Check_sidebar mb_35">
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">M</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">L</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">Xl</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">XXl</span>
                                    </label>
                                </li>
                                <li>
                                    <label class="primary_checkbox d-flex">
                                        <input type="checkbox">
                                        <span class="checkmark mr_10"></span>
                                        <span class="label_name">XXXL</span>
                                    </label>
                                </li>
                            </ul>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                            Filter by Rating
                            </h4>
                            <ul class="rating_lists mb_35">
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                    </div>
                                </li>
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <span>And Up</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <span>And Up</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <span>And Up</span>
                                    </div>
                                </li>
                                <li>
                                    <div class="ratings">
                                        <i class="fas fa-star"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <i class="fas fa-star unrated"></i>
                                        <span>And Up</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                                Filter by Colors
                            </h4>
                            <div class="color_filter">
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox black_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox blue_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox gray_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox paste_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox brouwn_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox violate_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter red_check">
                                    <label class="round_checkbox d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="single_coulorFilter">
                                    <label class="round_checkbox yellow_check d-flex">
                                        <input name="color_filt" type="radio">
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="single_pro_categry">
                            <h4 class="font_18 f_w_700">
                            Filter by Price
                            </h4>
                            <div class="filter_wrapper">
                                <div id="slider-range"></div>
                                <div class="d-flex align-items-center prise_line">
                                    <button class="theme_btn small_btn2">Filter</button>
                                    <span>Price: </span><input type="text" id="amount" readonly >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-xl-9">
                <div class="row ">
                    <div class="col-12">
                        <div class="box_header d-flex flex-wrap align-items-center justify-content-between">
                            <h5 class="font_16 f_w_500">Showing 1–12 of 40 Results</h5>
                            <div class="box_header_right">
                                <div class="short_select d-flex align-items-center">
                                    <div class="prduct_showing_style">

                                    </div>
                                    <select class="small_select" >
                                        <option data-display="Default Shorting">Default Shorting</option>
                                        <option value="1">Prise</option>
                                        <option value="2">Date</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters grid_shorting_style mb_50">
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/1.png" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/2.png" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/3.png" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/5.jpg" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/6.jpg" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/7.jpg" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="product_details.php">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/1.png" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/2.png" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/3.png" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                                <i class="ti-eye"></i>
                                            </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.phproduct_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/5.jpg" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/6.jpg" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                        <div class="product_wized">
                            <div class="thumb">
                                <img src="img/product/7.jpg" alt="">
                                <div class="product_action">
                                    <div class="product_action_inner">
                                        <a href="#">
                                            <i class="ti-heart"></i>
                                        </a>
                                        <a href="#">
                                            <i class="ti-control-shuffle"></i>
                                        </a>
                                        <a data-toggle="modal" data-target="#theme_modal">
                                            <i class="ti-eye"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="product_wized_body">
                                <div class="stars">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                </div>
                                <span class="mute_text d-block font_12 f_w_400">Interior Design</span>
                                <a href="product_details.php">
                                    <h4 class="font_18 f_w_700">Living Room Re-imagined</h4>
                                </a>
                                <p class="font_16  theme_text f_w_500">$180.00</p>
                            </div>
                            <a href="#" class="add_to_cart">Add to Cart</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <ul class="pagination_lists">
                            <li><a class="active" href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"> <i class="fas fa-chevron-right"></i> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- prodcuts_area ::end  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>