<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>News Details</h3>
                        <p><a href="index.php">Home </a>/ News Details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- blog_details_area::start  -->
<div class="blog_details_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <div class="blog_details_inner">
                    <h3 class="mb_30">Crafting beautiful stuff with our own 
                        hands and the useful tool.</h3>
                    <p class="mb_25">Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Sed ut perspiciatis.Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eu fugiat nulla pariatur. </p>
                    <p>Excepteur sint occaecat cupitataute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur.</p>
                    <div class="blog_details_banner">
                        <img src="img/blog_details/blog_banner.jpg" alt="">
                    </div>
                    <p class="mb_25">Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum Sed ut perspiciatis.Duis aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eu fugiat nulla pariatur. </p>
                    <p>Excepteur sint occaecat cupitataute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur aute irure dolor in reprehenderit in voluptate velit esseresti cillum dolore eues fugiat nulla pariatur.</p>
                    <div class="quote_text">
                        <div class="horizontal_line"></div>
                        <h4>Risus commodo viverra maecenas accumsan lacus velesinm facilisis ipsum dolor sit amet, consectetur adipiscing elitsed eiusmod tempor incididunte viverra maecenas accumsan lacus velesinm.</h4>
                    </div>
                    <div class="details_info">
                        <h4>The Cycleing Extraterrestrial </h4>
                        <p class="mb_25">Duis aute irure dolor reprehenderit  voluptate velit esse cillum dolore eu fugiatnulla xcepteur sint aecat cupidatat nones proident, sunt in culpa qui officiat mollit anim idestborum. Sedutes perspiciatis unde omnis iste natus error sitluptatem  enim ad minim veniamquis nostrud exercitation perspiciatis unde omnis iste natus error sit voluptatem.</p>
                        <p>Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute  reprehenderit inluptatee cillum dolore eugiatnulla xcepteur sint aecat cupidatat nones proident, sunt in culpa qui officiat mollit anim idestborumvelit esse cillume cillum dolore eu fugiatnulla xcepteur sint aecat cupidatat nones proident.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog_details_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>