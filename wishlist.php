<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>My Wishlist</h3>
                        <p><a href="index.php">Home </a>/ My Wishlist</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- my_order_area::start  -->
<div class="my_order_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-10">
                <h3 class="font_22 f_w_700 mb_30">My Wishlist</h3>
                <div class="table-responsive">
                        <table class="table custom_table mb-0">
                            <thead>
                                <tr>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Availability</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="product_name d-flex align-items-center">
                                            <div class="close_icon">
                                                <svg id="icon6" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                    <path data-name="Path 174" d="M0,0H16V16H0Z" fill="none"></path>
                                                    <path data-name="Path 175" d="M14.95,6l-1-1L9.975,8.973,6,5,5,6,8.973,9.975,5,13.948l1,1,3.973-3.973,3.973,3.973,1-1L10.977,9.975Z" transform="translate(-1.975 -1.975)" fill="#fb1159"></path>
                                                </svg>
                                            </div>
                                            <div class="thumb">
                                                <img src="img/cart/cart_products_1.png" alt="">
                                            </div>
                                            <span>The Unbundled University</span>
                                        </div>
                                    </td>
                                    <td class="f_w_400">$169.00</td>
                                    <td>
                                        <span class="in_stoke">In Stock</span>
                                    </td>
                                    <td class="f_w_600">
                                        <a href="product.php" class="theme_btn small_btn4">View Product</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="product_name d-flex align-items-center">
                                            <div class="close_icon">
                                                <svg id="icon5" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                    <path data-name="Path 174" d="M0,0H16V16H0Z" fill="none"></path>
                                                    <path data-name="Path 175" d="M14.95,6l-1-1L9.975,8.973,6,5,5,6,8.973,9.975,5,13.948l1,1,3.973-3.973,3.973,3.973,1-1L10.977,9.975Z" transform="translate(-1.975 -1.975)" fill="#fb1159"></path>
                                                </svg>
                                            </div>
                                            <div class="thumb">
                                                <img src="img/cart/cart_products_2.png" alt="">
                                            </div>
                                            <span>The Unbundled University</span>
                                        </div>
                                    </td>
                                    <td class="f_w_400">$169.00</td>
                                    <td>
                                        <span class="in_stoke">In Stock</span>
                                    </td>
                                    <td class="f_w_600">
                                        <a href="product.php" class="theme_btn small_btn4">View Product</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="product_name d-flex align-items-center">
                                            <div class="close_icon">
                                                <svg id="icon4" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                    <path data-name="Path 174" d="M0,0H16V16H0Z" fill="none"></path>
                                                    <path data-name="Path 175" d="M14.95,6l-1-1L9.975,8.973,6,5,5,6,8.973,9.975,5,13.948l1,1,3.973-3.973,3.973,3.973,1-1L10.977,9.975Z" transform="translate(-1.975 -1.975)" fill="#fb1159"></path>
                                                </svg>
                                            </div>
                                            <div class="thumb">
                                                <img src="img/cart/cart_products_3.png" alt="">
                                            </div>
                                            <span>The Unbundled University</span>
                                        </div>
                                    </td>
                                    <td class="f_w_400">$169.00</td>
                                    <td>
                                        <span class="in_stoke">In Stock</span>
                                    </td>
                                    <td class="f_w_600">
                                        <a href="product.php" class="theme_btn small_btn4">View Product</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <div class="product_name d-flex align-items-center">
                                            <div class="close_icon">
                                                <svg id="icon3" xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
                                                    <path data-name="Path 174" d="M0,0H16V16H0Z" fill="none"></path>
                                                    <path data-name="Path 175" d="M14.95,6l-1-1L9.975,8.973,6,5,5,6,8.973,9.975,5,13.948l1,1,3.973-3.973,3.973,3.973,1-1L10.977,9.975Z" transform="translate(-1.975 -1.975)" fill="#fb1159"></path>
                                                </svg>
                                            </div>
                                            <div class="thumb">
                                                <img src="img/cart/cart_products_3.png" alt="">
                                            </div>
                                            <span>The Unbundled University</span>
                                        </div>
                                    </td>
                                    <td class="f_w_400">$169.00</td>
                                    <td>
                                        <span class="in_stoke">In Stock</span>
                                    </td>
                                    <td class="f_w_600">
                                        <a href="product.php" class="theme_btn small_btn4">View Product</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
            </div>
        </div>
    </div>
</div>
<!-- my_order_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>