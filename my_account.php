<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>My Account</h3>
                        <p><a href="index.php">Home </a>/ My Account</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- my_account_area::start  -->
<div class="my_account_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <h3 class="font_30 f_w_700 mb_45">My Account</h3>
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="single_account mb_30">
                            <div class="icon">
                                <img src="img/my_account/1.svg" alt="">
                            </div>
                            <h4>My Orders</h4>
                            <p>Eiusmod tempor incididunt ut labore et dolore 
                            magna rester about the is a aliqua.</p>
                            <a href="my_order.php" class="arrow_btn">See Order »</a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single_account mb_30">
                            <div class="icon">
                                <img src="img/my_account/address.svg" alt="">
                            </div>
                            <h4>Address</h4>
                            <p>Eiusmod tempor incididunt ut labore et dolore 
                            magna rester about the is a aliqua.</p>
                            <a href="my_address.php" class="arrow_btn">Update »</a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single_account mb_30">
                            <div class="icon">
                                <img src="img/my_account/account.svg" alt="">
                            </div>
                            <h4>Account Details</h4>
                            <p>Eiusmod tempor incididunt ut labore et dolore 
                            magna rester about the is a aliqua.</p>
                            <a href="account_info.php" class="arrow_btn">Update »</a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="single_account mb_30">
                            <div class="icon">
                                <img src="img/my_account/support.svg" alt="">
                            </div>
                            <h4>Support Center</h4>
                            <p>Eiusmod tempor incididunt ut labore et dolore 
                            magna rester about the is a aliqua.</p>
                            <a href="contact.php" class="arrow_btn">Get Support »</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- my_account_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>