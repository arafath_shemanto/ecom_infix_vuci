<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Wichita Hydraulic Table</h3>
                        <p><a href="index.php">Home </a>/ <a href="product.php">New Arrivals</a> /  Product details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- checkout_wrapper_area::start   -->
<div class="checkout_wrapper_area">
    <div class="container">
        <div class="row justify-content-end">
            <div class="col-xl-11">
                <div class="checkout_wrapper">
                    <div class="billing_details_wrapper">
                        <h3 class="font_22 f_w_700 mb_30">Billing Details</h3>
                        <form action="#">
                            <div class="row">
                                <div class="col-lg-6">
                                    <label class="primary_label2">First Name <span>*</span></label>
                                    <input name="name" placeholder="Enter First Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter First Name'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-6">
                                    <label class="primary_label2">Last Name <span>*</span></label>
                                    <input name="lname" placeholder="Enter Last Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Last Name'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label2">Company Name (Optional)</label>
                                    <input name="cname" placeholder="Enter Company Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Company Name'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label2">Country <span>*</span> </label>
                                    <select class="theme_select wide mb_20">
                                        <option value="0">Bangladesh</option>
                                        <option value="1">USA</option>
                                    </select>
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label2">Street Address <span>*</span></label>
                                    <input name="sname" placeholder="House Number and street address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'House Number and street address'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12">
                                    <input name="cname" placeholder="Apartment, suite, unit etc (Optional)" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Apartment, suite, unit etc (Optional)'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label2">City / Town *</label>
                                    <input name="cname" placeholder="Enter city/town name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter city/town name'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label2" >District <span>*</span> </label>
                                    <select class="theme_select wide mb_20">
                                        <option value="0">Dhaka</option>
                                        <option value="1">Rangpur</option>
                                        <option value="2">Rajshahi</option>
                                    </select>
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label2" >Postcode / ZIP (Optional)</label>
                                    <input name="cname" placeholder="Enter Company Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Company Name'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label2">Phone No (Optional)</label>
                                    <input name="cname" placeholder="01XXXXXXXXXX" onfocus="this.placeholder = ''" onblur="this.placeholder = '01XXXXXXXXXX'" class="primary_input3 mb_20" required="" type="text">
                                </div>
                                <div class="col-lg-12 mb_35">
                                    <label class="primary_label2" >Email Address <span>*</span></label>
                                    <input name="cname" placeholder="e.g example@domian.com" onfocus="this.placeholder = ''" onblur="this.placeholder = 'e.g example@domian.com'" class="primary_input3 mb_20" required="" type="email">
                                </div>
                                <div class="col-12">
                                    <h3 class="font_22 f_w_700 mb_23">Additional Information</h3>
                                </div>
                                <div class="col-lg-12">
                                    <label class="primary_label2">Information details</label>
                                    <textarea class="primary_textarea3"  placeholder="Note about your order, e.g. special note for you delivery" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Note about your order, e.g. special note for you delivery'"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="order_wrapper">
                        <h3 class="font_22 f_w_700 mb_30">Your order</h3>
                        <div class="ordered_products">
                            <div class="single_ordered_product">
                                <div class="product_name d-flex align-items-center">
                                    <div class="thumb">
                                        <img src="img/cart/small_img_1.png" alt="">
                                    </div>
                                    <span>The Unbundled University</span>
                                </div>
                                <span class="order_prise f_w_500 font_16">$169.00</span>
                            </div>
                            <div class="single_ordered_product">
                                <div class="product_name d-flex align-items-center">
                                    <div class="thumb">
                                        <img src="img/cart/small_img_2.png" alt="">
                                    </div>
                                    <span>The Unbundled University</span>
                                </div>
                                <span class="order_prise f_w_500 font_16">$169.00</span>
                            </div>
                        </div>
                        <div class="ordered_products_lists">
                            <div class="single_lists">
                                <span class=" total_text" >Subtotal</span>
                                <span>$12,000.00</span>
                            </div>
                            <div class="single_lists">
                                <span class="total_text" >Coupon Code</span>
                                <span class="remove_coupon_text" >remove</span>
                                <input class="coupon_value" value="6YRDZMD04" type="text">
                            </div>
                            <div class="single_lists">
                                <span class="total_text" >Grand Total  </span>
                                <span>$12,000.00</span>
                            </div>
                        </div>

                        <div class="shiping_billing_list">
                            <h4 class="font_18 f_w_600 mb_15">Shipping & Billing</h4>
                            <div class="single_shiping_billing mb-0">
                                <div class="single_shiping_Inner d-flex align-items-center">
                                    <div class="shiping_info d-flex align-items-center">
                                        <i class="ti-location-pin"></i> <h5 class="font_16 f_w_600 mb-0" >Robert Downey JR. </h5> 
                                    </div>
                                    <span class="font_12 f_w_500 theme_text text-uppercase" >Edit</span>
                                </div>
                                <p class="font_14 f_w_400 text_mute" >2593 Timbercrest Road, Chisana, Alaska 
                                Badalas United State.</p>
                            </div>
                            <div class="single_shiping_billing">
                                <div class="single_shiping_Inner d-flex align-items-center">
                                    <div class="shiping_info d-flex align-items-center">
                                        <i class="ti-clipboard"></i> <h5 class="font_16 f_w_600 mb-0" >Bill to the same address</h5> 
                                    </div>
                                    <span class="font_12 f_w_500 theme_text text-uppercase" >Edit</span>
                                </div>
                            </div>
                            <div class="single_shiping_billing">
                                <div class="single_shiping_Inner d-flex align-items-center">
                                    <div class="shiping_info d-flex align-items-center">
                                        <i class="ti-headphone-alt"></i> <h5 class="font_16 f_w_600 mb-0" >+880 1841136251</h5> 
                                    </div>
                                    <span class="font_12 f_w_500 theme_text text-uppercase" >Edit</span>
                                </div>
                            </div>
                            <div class="single_shiping_billing">
                                <div class="single_shiping_Inner d-flex align-items-center">
                                    <div class="shiping_info d-flex align-items-center">
                                        <i class="ti-email"></i> <h5 class="font_16 f_w_600 mb-0" >info@spondonit.com</h5> 
                                    </div>
                                    <span class="font_12 f_w_500 theme_text text-uppercase" >Edit</span>
                                </div>
                            </div>
                        </div>
                        
                        <div class="bank_transfer">
                            <div class="bank_check">
                                <label class="primary_bulet_checkbox d-inline-flex">
                                    <input name="qus" checked type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name" >Direct Bank Transfer</span>
                                </label>
                            </div>
                            <div class="quote">
                                <p>Make your payment directly into our bank account Please use your Order ID as the payment reference. Your order will not be shipped until the funds have cleared 
                                    in our account.</p>
                            </div>
                            <div class="payment">
                                <label class="primary_bulet_checkbox d-inline-flex">
                                    <input name="qus" checked type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name">Offline Payment</span>
                                </label>
                                <label class="primary_bulet_checkbox d-inline-flex">
                                    <input name="qus" checked type="checkbox">
                                    <span class="checkmark mr_10"></span>
                                    <span class="label_name" >PayPal</span>
                                    <img src="img/cart/paypal.png" alt="">
                                </label>
                            </div>
                            <p class="mb_35">Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our privacy policy.</p>
                            <button class="theme_btn w-100">Place An Order</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- checkout_wrapper_area::end   -->


<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>