<?php include 'include/header.php' ?>
<!-- template_signup_wrapper::start  -->
<div class="template_signup_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="logo text-center">
                    <a href="#">
                        <img src="img/logo.png" alt="">
                    </a>
                </div>
            </div>
            <div class="col-12">
                <div class="banner template_bg_1">
                    <div class="banner_text">
                        <h3>One Small Step For 
                            One Giant Leap.</h3>
                        <a href="#" class="theme_btn">Shop Now</a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="template_main_content text-center">
                    <h3 class="font_24 f_w_700 mb_15" >Your Account is Registered!</h3>
                    <p class="f_w_500" >One small step for you, but one giant leap ahead for your shoppping <br>
                        experienceHere’s just a couple of things we have for you</p>
                    <div class="services_grid">
                        <div class="single_service">
                            <div class="icon">
                                <img src="img/template/1.svg" alt="">
                            </div>
                            <h4>Over 3 Million
                                    Products.</h4>
                        </div>
                        <div class="single_service">
                            <div class="icon">
                                <img src="img/template/2.svg" alt="">
                            </div>
                            <h4>Best Prices We
                            Provide</h4>
                        </div>
                        <div class="single_service">
                            <div class="icon">
                                <img src="img/template/3.svg" alt="">
                            </div>
                            <h4>Ease and Speed
                            Process</h4>
                        </div>
                        <div class="single_service">
                            <div class="icon">
                                <img src="img/template/4.svg" alt="">
                            </div>
                            <h4>100% Protected
                                Service</h4>
                        </div>
                    </div>
                    <div class="template_contact text-center">
                        <h4 class="font_24 f_w_700 mb_15" >Need Help?</h4>
                        <p class="mb_25 f_w_500">Happy to assist you. Our representatives are available from <br>
                        9:30am to 9:30pm, 7 days a week.</p>
                        <a href="#" class="theme_btn">Contact Us</a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="themplate_download_app text-center">
                    <h4 class="font_24 f_w_700 mb-0">Save More on App!</h4>
                    <p class="f_w_500">Get exclusive discounts, voucher & daily <br>
                        flash sales on the app.</p>
                    <div class="download_Links">
                        <a href="#">
                            <img src="img/template/google.svg" alt="">
                        </a>
                        <a href="#">
                            <img src="img/template/apple.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="template_footer">
                    <div class="template_footer_text">
                        <p>You have received this email because you or someone else has confirmed the email address. This would like 
                        to receive email communication from InfixVuci. We will never share your personal information 
                        (such as your email address with any other 3rd party without your consent).</p>
                        <p>This email was sent by: Tajwar Centre House No: 40 Baria Sreet 133/2 NY City, United States.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- template_signup_wrapper::end  -->


<?php include 'include/footer.php' ?>