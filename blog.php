<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>
<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Latest News</h3>
                        <p><a href="index.php">Home </a>/  Latest news</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area:: end  -->

<!-- blog_page_area:: start  -->
<div class="blog_page_area">
    <div class="container">
        <div class="row">
            <div class="col-xl-5">
                <div class="blog_big_img">
                    <!-- <a href="blog_details.php" class="thumb">
                        <img src="img/blog/blog_big.jpg" alt="">
                    </a> -->
                    <div class="blog_meta">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>Thinking beyond influent social 
                            fashion style icon.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-7">
                <div class="blog_page_gallry">
                    <!-- single_blog_galry  -->
                    <div class="single_blog_galry">
                        <a href="blog_details.php" class="thumb">
                            <img src="img/blog/4.jpg" alt="">
                        </a>
                        <div class="blog_content">
                            <span>April 08, 2019  •  Fashion</span>
                            <a href="blog_details.php">
                                <h4>Complex Legacy of Vigilantism 
                                in South Africa.</h4>
                            </a>
                        </div>
                    </div>
                    <!-- single_blog_galry  -->
                    <div class="single_blog_galry">
                        <a href="blog_details.php" class="thumb">
                            <img src="img/blog/5.jpg" alt="">
                        </a>
                        <div class="blog_content">
                            <span>April 08, 2019  •  Fashion</span>
                            <a href="blog_details.php">
                                <h4>Complex Legacy of Vigilantism 
                                in South Africa.</h4>
                            </a>
                        </div>
                    </div>
                    <!-- single_blog_galry  -->
                    <div class="single_blog_galry">
                        <a href="blog_details.php" class="thumb">
                            <img src="img/blog/6.jpg" alt="">
                        </a>
                        <div class="blog_content">
                            <span>April 08, 2019  •  Fashion</span>
                            <a href="blog_details.php">
                                <h4>Complex Legacy of Vigilantism 
                                in South Africa.</h4>
                            </a>
                        </div>
                    </div>
                    <!-- single_blog_galry  -->
                    <div class="single_blog_galry">
                        <a href="blog_details.php" class="thumb">
                            <img src="img/blog/7.jpg" alt="">
                        </a>
                        <div class="blog_content">
                            <span>April 08, 2019  •  Fashion</span>
                            <a href="blog_details.php">
                                <h4>Complex Legacy of Vigilantism 
                                in South Africa.</h4>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog_page_area::end  -->

<!-- blog_area::start  -->
<div class="blog_area blog_area_page">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="main_title mb_60 text-center">
                    <h3 class="mb-0">Form Our Blog Post</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/1.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>Thinking beyond influent social 
                                fashion style icon.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/2.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>The Complex Legacy of Vigilantism 
                            in South Africa.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/3.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>What It Would Take for Evangelicals 
                            to Turn on Trump.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/1.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>Thinking beyond influent social 
                                fashion style icon.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/2.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>The Complex Legacy of Vigilantism 
                            in South Africa.</h4>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <div class="single_blog">
                    <a href="blog_details.php" class="thumb">
                        <img src="img/blog/3.jpg" alt="">
                    </a>
                    <div class="blog_content">
                        <span>April 08, 2019  •  Fashion</span>
                        <a href="blog_details.php">
                            <h4>What It Would Take for Evangelicals 
                            to Turn on Trump.</h4>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- blog_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>