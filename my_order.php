<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>My Orders</h3>
                        <p><a href="index.php">Home </a>/ My Account</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- my_order_area::start  -->
<div class="my_order_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-8">
                <h3 class="font_30 f_w_700 mb_30">My Order</h3>
                <div class="table-responsive">
                    <table class="table my_order_table mb-0">
                        <thead>
                            <tr>
                            <th scope="col">Order No.</th>
                            <th scope="col">Date</th>
                            <th scope="col">Status</th>
                            <th scope="col">Items</th>
                            <th scope="col">Total</th>
                            <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                <td>26 May 2020</td>
                                <td> <span class="badge_1">Pending</span> </td>
                                <td>02</td>
                                <td>$360.00</td>
                                <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                            </tr>
                            <tr>
                                <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                <td>26 May 2020</td>
                                <td> <span class="badge_2">Received</span> </td>
                                <td>02</td>
                                <td>$360.00</td>
                                <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                            </tr>
                            <tr>
                                <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                <td>26 May 2020</td>
                                <td> <span class="badge_1">Pending</span> </td>
                                <td>02</td>
                                <td>$360.00</td>
                                <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                            </tr>
                            <tr>
                                <td > <span class="theme_text f_w_500" >#6YRDZMD04</span></td>
                                <td>26 May 2020</td>
                                <td> <span class="badge_2">Received</span> </td>
                                <td>02</td>
                                <td>$360.00</td>
                                <td> <a href="order_details.php" class="action_text theme_text text_underline">Show Order</a> </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- my_order_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>