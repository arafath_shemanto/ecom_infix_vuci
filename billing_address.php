<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Update Billing Address</h3>
                        <p><a href="index.php">Home </a>/ My Account / Update Billing Address</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- billing_address_area::start  -->
<div class="billing_address_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-10">
                <h3 class="font_30 f_w_700 mb_45">Update Billing Address</h3>
                <form action="#">
                    <div class="row">
                        <div class="col-12">
                            <label class="primary_label">Address *</label>
                            <input class="primary_input3 mb_20" placeholder="Address 01" type="text">
                            <input class="primary_input3 mb_18" placeholder="Address 02" type="text">
                        </div>
                        <div class="col-12">
                            <label class="primary_label">City *</label>
                            <input class="primary_input3 mb_18" placeholder="Enter city name" type="text">
                        </div>
                        <div class="col-12">
                            <label class="primary_label">State *</label>
                            <input class="primary_input3 mb_18" placeholder="Enter state name" type="text">
                        </div>
                        <div class="col-12">
                            <label class="primary_label">Postcode *</label>
                            <input class="primary_input3 mb_18" placeholder="Enter postcode" type="text">
                        </div>
                        <div class="col-12">
                            <label class="primary_label">Country *</label>
                            <input class="primary_input3 mb_20" placeholder="Enter country name" type="text">
                        </div>
                        <div class="col-12">
                            <a href="#" class="theme_btn">Update Address</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- billing_address_area::end  -->


<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>