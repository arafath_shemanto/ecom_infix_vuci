<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- breadcrumb_area::start  -->
<div class="breadcrumb_area">
    <div class="container">
        <div class="breadcrumb_iner bradcam_bg_2">
            <div class="bradcam_text">
                <div class="row justify-content-end">
                    <div class="col-lg-6">
                        <h3>Career</h3>
                        <p><a href="index.php">Home </a>/ career Details</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb_area::end  -->

<!-- career_details_area::start  -->
<div class="career_details_area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-8">
                <div class="career_details_top">
                    <span class="job_tag theme_btn small_btn4">Full Time</span>
                    <div class="career_details_info">
                        <div class="single_details_info">
                            <i class="ti-location-pin"></i>
                            <span>2593 Timbercrest Road, Chisana</span>
                        </div>
                        <div class="single_details_info">
                            <i class="ti-user"></i>
                            <span>No. of Vacancies: 2</span>
                        </div>
                        <div class="single_details_info">
                            <i class="ti-bookmark-alt"></i>
                            <span>Experience: 1 - 3 Year</span>
                        </div>
                    </div>
                </div>
                <div class="career_details_banner">
                    <img src="img/career/details_banner.jpg" alt="">
                </div>
                <div class="row">
                    <div class="col-xl-11">
                            <p>We are seeking two talented Creative UI Designers to join our creative team. <br>
                        – Both positions are not for any Fresher.</p>
                        <div class="note_text">
                            <p>NB: Currently we are working from home due to COVID 19. You have to work from home with tracking tools. 
                            But this is a full-time office job after COVID 19</p>
                        </div>
                        <div class="career_details_list">
                            <h4 class="font_22">Job Description / Responsibility: –</h4>
                            <ul class="details_lists">
                                <li>– Deeply passionate about design. You see Design as a problem-solving-discipline, not Art</li>
                                <li>– Has an impressive track record and an outstanding Visual design portfolio</li>
                                <li>– Bring ideas and concepts to life through creative designs</li>
                                <li>– Execute all visual design stages from concept to final hand-off to engineering</li>
                                <li>– Analyze and extend the features of our existing products in terms of usability and UI</li>
                                <li>– Collaborate closely with Product and Design to develop long term research plans</li>
                                <li>– Adapt and innovate research methods to deliver the right insights for the problem</li>
                                <li>– Translate concepts into user flows, wireframes, mockups, and prototypes that lead to intuitive user experiences.</li>
                                <li>– Translate concepts into user flows, wireframes, mockups, and prototypes that lead to intuitive user experiences.</li>
                                <li>– You regularly study new things</li>
                            </ul>
                        </div>
                        <div class="career_details_list">
                            <h4 class="font_22">Requirements: –</h4>
                            <ul class="details_lists">
                                <li>– Demonstrable UI design skills with a strong portfolio</li>
                                <li>– Excellent visual design skills with sensitivity to user-system interaction</li>
                                <li>– Up-to-date with the latest UI trends, techniques, and technologies</li>
                                <li>– Experience working in an Agile/Scrum development process</li>
                            </ul>
                        </div>
                        <div class="career_details_list">
                            <h4 class="font_22">Working Days: –</h4>
                            <ul class="details_lists">
                                <li>– Weekly 5 days. Sunday to Thursday. 10 AM to 7 PM</li>
                            </ul>
                        </div>
                        <div class="career_details_list">
                            <h4 class="font_22">Salary Range: –</h4>
                            <ul class="details_lists">
                                <li>–   35,000 to 50,000 USD (Based on your Experience )</li>
                            </ul>
                        </div>
                        <div class="career_details_list">
                            <h4 class="font_22">Benefits you’ll get: –</h4>
                            <ul class="details_lists">
                                <li>– A great product and a highly motivated and experienced team that wants to push boundaries</li>
                                <li>– Two festival bonus (According to the company policy)</li>
                                <li>– Training and learning materials to improve skills.</li>
                                <li>– Lunch and Snacks will be provided from the office.</li>
                                <li>– Unlimited tea and coffee.</li>
                                <li>– Fun, Relaxed Working Environment</li>
                                <li>– Annual Salary Increments According to performance</li>
                            </ul>
                        </div>
                        <p class="appliication_deadline" ><span>Application Deadline:</span> November 30, 2020</p>
                        <a href="apply_job.php" class="theme_btn">Apply For This Position</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- career_details_area::end  -->

<!-- cta_area::start  -->
<div class="cta_area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="cta_inner">
                    <div class="cta_inner_text">
                        <h4><span>New Deals</span>
                        Start Daily at 12pm e.t.</h4>
                        <div class="cta_text">
                            <p>Get  <span class="text-uppercase theme_text f_w_600" >FREE SHIPPING* & 5% rewards </span> on <br>
                            every order with <span class="f_w_600"  >InfixVuci Theme</span> rewards program</p>
                        </div>
                    </div>
                    <div class="cta_inner_subscribe">
                        <div class="subcribe-form theme_mailChimp"  id="mc_embed_signup">
                            <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                                method="get" class="subscription relative">
                                <input name="EMAIL" class="form-control" placeholder="Type e-mail  address here" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Type e-mail  address here'"
                                    required="" type="email">
                                <div style="position: absolute; left: -5000px;">
                                    <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                </div>
                                <button class="">Subscribe</button>
                                <div class="info"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta_area::end  -->

<?php include 'include/footer_content.php' ?>
<?php include 'include/footer.php' ?>