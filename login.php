<?php include 'include/header.php' ?>
<?php include 'include/menu.php' ?>

<!-- infix_login_area::start  -->
<div class="infix_login_area">
    <div class="login_area_inner">
        <h4>Welcome back, Please login
            to your account </h4>
        <div class="login_with_links">
            <a href="#"> <i class="fab fa-google"></i> Login with google</a>
            <a class="Facebook_bg" href="#"> <i class="ti-facebook"></i>Login with Facebook</a>
            <a class="twitter_bg" href="#"> <i class="ti-twitter-alt"></i> Login with google</a>
        </div>
        <p class="sign_up_text mb_40">Or login with Email Address</p>
        <form action="#">
            <div class="row">
                <div class="col-12">
                    <div class="input-group custom_group_field mb_35">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <img src="img/my_account/email.svg" alt="">
                            </span>
                        </div>
                        <input type="email" class="form-control" placeholder="E.g. example@gmail.com" aria-label="E.g. example@gmail.com" >
                    </div>
                </div>
                <div class="col-12">
                    <div class="input-group custom_group_field ">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <img src="img/my_account/pass.svg" alt="">
                            </span>
                        </div>
                        <input type="password" class="form-control" placeholder="Enter Password" aria-label="Enter Password" >
                    </div>
                </div>
                <div class="col-12">
                    <div class="remember_pass mb_35">
                        <label class="primary_checkbox d-flex">
                            <input checked="" type="checkbox">
                            <span class="checkmark mr_15"></span>
                            <span class="label_name">Remember Me</span>
                        </label>
                        <a class="forgot_pass" href="#">Forgot Password</a>
                    </div>
                </div>
                <div class="col-12">
                    <button class="theme_btn w-100 text-center">Sign In</button>
                </div>
                <div class="col-12">
                    <p class="sign_up_text" >Don’t have an account? <a href="resister.php">Sing Up</a></p>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- infix_login_area::end  -->

<?php include 'include/footer.php' ?>